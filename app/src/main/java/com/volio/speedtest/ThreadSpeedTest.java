package com.volio.speedtest;

import android.location.Location;
import android.util.Log;

import com.volio.speedtest.test.HttpDownloadTest;
import com.volio.speedtest.test.HttpUploadTest;
import com.volio.speedtest.test.PingTest;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class ThreadSpeedTest extends Thread {
    private GetSpeedTestHostsHandler getSpeedTestHostsHandler=null;
    private HttpUploadTest httpUploadTest;
    private HttpDownloadTest httpDownloadTest;
    private PingTest pingTest;
    private String mStartButtonString;
    private Boolean mStartButtonStatus = true;
    private String mToastNotificatonString;
    private int mStartButtonSize = 12;
    private Boolean allFinished = false;
    private double selfLat = 0.0;
    private double selfLon = 0.0;
    private final DecimalFormat dec = new DecimalFormat("#.##");
    private String mPingTextViewString = "0 ms";
    private String mUploadTextViewString = "0 Mbps";
    private String mDownloadTextViewString = "0 Mbps";
    private float position = 0;
    private float lastPosition = 0;
    private String mDateTimeString;
    private String mExternalIp;
    private String mInternalIp;
    private static final String FILECACHE="speed_test_data.dat";
    private  List<HistoryItem> mListHistoryItems=new ArrayList<>();
    public String getmExternalIp() {
        return mExternalIp;
    }
    @Override
    public void run() {
        mStartButtonStatus = false;
        if (getSpeedTestHostsHandler == null) {
            restartSpeedTestHostsHandler();
        }
        mStartButtonString = "Selecting best server based on Distance...";
        int timeCount = 600;
        while (!getSpeedTestHostsHandler.isFinished()) {
            timeCount--;
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
                mToastNotificatonString = "No Connnection...";
                    if (timeCount < 0) {
                        mStartButtonStatus = true;
                mStartButtonString = "RESTART TEST";
                mStartButtonSize = 16;
                getSpeedTestHostsHandler = null;
                return;
            }

        }
        HashMap<Integer, String> mapKey = getSpeedTestHostsHandler.getMapKey();
        HashMap<Integer, List<String>> mapValue = getSpeedTestHostsHandler.getMapValue();
        if (selfLat == 0.0) {
            selfLat = getSpeedTestHostsHandler.getSelfLat();
            selfLon = getSpeedTestHostsHandler.getSelfLon();

        }
        double dist = 0.0;
        int findServerIndex = 0;
        double tmp = 19349458;
        for (int index : mapKey.keySet()) {

            Location source = new Location("Source");
            source.setLatitude(selfLat);
            source.setLongitude(selfLon);

            List<String> ls = mapValue.get(index);
            Location dest = new Location("Dest");

            dest.setLatitude(Double.parseDouble(ls.get(0)));
            dest.setLongitude(Double.parseDouble(ls.get(1)));
            double distance = source.distanceTo(dest);
            if (tmp > distance) {
                tmp = distance;
                dist = distance;
                findServerIndex = index;
            }
        }
        final String uploadAddr = mapKey.get(findServerIndex);
        final List<String> info = mapValue.get(findServerIndex);
        final double distance = dist;
        Log.e("quay len", info.get(6) + " " + selfLat + " " + selfLon);
        mStartButtonSize = 13;
        mStartButtonString = String.format("Host Location: %s [Distance: %s km]", info.get(2), dec.format(distance / 1000));
        mPingTextViewString = "0 ms";
        mUploadTextViewString = "0 Mbps";
        mDownloadTextViewString = "0 Mbps";
        final List<Double> pingRateList = new ArrayList<>();
        final List<Double> downloadRateList = new ArrayList<>();
        final List<Double> uploadRateList = new ArrayList<>();
        Boolean pingTestStarted = false;
        Boolean pingTestFinished = false;
        Boolean downloadTestStarted = false;
        Boolean downloadTestFinished = false;
        Boolean uploadTestStarted = false;
        Boolean uploadTestFinished = false;
        final PingTest pingTest = new PingTest(info.get(6).replace(":8080", ""), 6);
        final HttpDownloadTest downloadTest = new HttpDownloadTest(uploadAddr.replace(uploadAddr.split("/")[uploadAddr.split("/").length - 1], ""));
        final HttpUploadTest uploadTest = new HttpUploadTest(uploadAddr);
        while (true) {
            if (!pingTestStarted) {
                pingTest.start();
                pingTestStarted = true;
            }
            if (pingTestFinished && !downloadTestStarted) {
                downloadTest.start();
                downloadTestStarted = true;
            }
            if (downloadTestFinished && !uploadTestStarted) {
                uploadTest.start();
                uploadTestStarted = true;
            }
            if (pingTestFinished) {
                if (pingTest.getAvgRtt() == 0) {
                    System.out.println("Ping error");
                } else {
                    mPingTextViewString = dec.format(pingTest.getAvgRtt()) + " ms";
                }
            } else {
                pingRateList.add(pingTest.getInstantRtt());
                mPingTextViewString = dec.format(pingTest.getInstantRtt()) + " ms";
            }
            //Download Test
            if (pingTestFinished) {
                if (downloadTestFinished) {
                    //Failure
                    if (downloadTest.getFinalDownloadRate() == 0) {
                        System.out.println("Download error...");
                    } else {
                        //Success
                        mDownloadTextViewString = dec.format(downloadTest.getFinalDownloadRate()) + " Mbps";
                    }
                } else {
                    //Calc position
                    double downloadRate = downloadTest.getInstantDownloadRate();
                    downloadRateList.add(downloadRate);
                    position = getPositionByRate(downloadRate);
                    mDownloadTextViewString = dec.format(downloadTest.getInstantDownloadRate()) + " Mbps";
                    lastPosition = position;
                }

                //Update chart

            }//upload test
            if (downloadTestFinished) {
                if (uploadTestFinished) {
                    //Failure
                    if (uploadTest.getFinalUploadRate() == 0) {
                        System.out.println("Upload error...");
                    } else {
                        //Success
                        mUploadTextViewString = dec.format(uploadTest.getFinalUploadRate()) + " Mbps";

                    }
                } else {
                    //Calc position
                    double uploadRate = uploadTest.getInstantUploadRate();
                    uploadRateList.add(uploadRate);
                    position = getPositionByRate(uploadRate);
                    mUploadTextViewString = dec.format(uploadTest.getInstantUploadRate()) + " Mbps";
                    lastPosition = position;

                }
            }
            //Test bitti
            if (pingTestFinished && downloadTestFinished && uploadTest.isFinished()) {
                break;
            }

            if (pingTest.isFinished()) {
                pingTestFinished = true;
            }
            if (downloadTest.isFinished()) {
                downloadTestFinished = true;
            }
            if (uploadTest.isFinished()) {
                uploadTestFinished = true;
            }

            if (pingTestStarted && !pingTestFinished) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                }
            } else {
                try {
                    Thread.sleep(100);
                }
                catch (InterruptedException e) {
                }
            }
            mInternalIp = getSpeedTestHostsHandler.getmIP();

        }
        mStartButtonString="Restart Test";
        mStartButtonSize=16;
        mStartButtonStatus=true;
        mUploadTextViewString=dec.format(uploadTest.getFinalUploadRate()) + "Mbps";
        mDownloadTextViewString=dec.format(downloadTest.getFinalDownloadRate()) + "Mbps";
        mPingTextViewString=pingTest.getAvgRtt() + "Mbps";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date();
        mDateTimeString = formatter.format(date);
        try (
                final DatagramSocket socket = new DatagramSocket())

        {
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            mExternalIp = socket.getLocalAddress().getHostAddress();
        } catch (
                SocketException e)

        {
            e.printStackTrace();
        } catch (
                UnknownHostException e)

        {
            e.printStackTrace();
        }
        mInternalIp = getSpeedTestHostsHandler.getmIP();
        allFinished=true;
    }

    public String getmInternalIp() {
        return mInternalIp;
    }

    public double getSelfLat() {
        return selfLat;
    }

    public double getSelfLon() {
        return selfLon;
    }

    public String getmDateTimeString() {
        return mDateTimeString;
    }

    public float getPosition() {
        return position;
    }

    public float getLastPosition() {
        return lastPosition;
    }

    public float getPositionByRate(double rate) {
        if (rate >= 0 && rate <= 120) {
            return (float) rate * 2;
        }
        return 0;
    }

    public String getmPingTextViewString() {
        return mPingTextViewString;
    }

    public String getmUploadTextViewString() {
        return mUploadTextViewString;
    }

    public String getmDownloadTextViewString() {
        return mDownloadTextViewString;
    }

    public void restartSpeedTestHostsHandler() {
        getSpeedTestHostsHandler = new GetSpeedTestHostsHandler();
        getSpeedTestHostsHandler.start();
    }

    public String getmToastNotificatonString() {
        return mToastNotificatonString;
    }

    public void setmToastNotificatonString(String mToastNotificatonString) {
        this.mToastNotificatonString = mToastNotificatonString;
    }

    public int getmStartButtonSize() {
        return mStartButtonSize;
    }

    public void setmStartButtonSize(int mStartButtonSize) {
        this.mStartButtonSize = mStartButtonSize;
    }

    public GetSpeedTestHostsHandler getGetSpeedTestHostsHandler() {
        return getSpeedTestHostsHandler;
    }

    public void setGetSpeedTestHostsHandler(GetSpeedTestHostsHandler getSpeedTestHostsHandler) {
        this.getSpeedTestHostsHandler = getSpeedTestHostsHandler;
    }

    public HttpUploadTest getHttpUploadTest() {
        return httpUploadTest;
    }

    public Boolean getAllFinished() {
        return allFinished;
    }

    public void setAllFinished(Boolean allFinished) {
        this.allFinished = allFinished;
    }

    public void setHttpUploadTest(HttpUploadTest httpUploadTest) {
        this.httpUploadTest = httpUploadTest;
    }

    public HttpDownloadTest getHttpDownloadTest() {
        return httpDownloadTest;
    }

    public void setHttpDownloadTest(HttpDownloadTest httpDownloadTest) {
        this.httpDownloadTest = httpDownloadTest;
    }

    public PingTest getPingTest() {
        return pingTest;
    }

    public void setPingTest(PingTest pingTest) {
        this.pingTest = pingTest;
    }

    public String getmStartButtonString() {
        return mStartButtonString;
    }

    public void setmStartButtonString(String mStartButtonString) {
        this.mStartButtonString = mStartButtonString;
    }

    public Boolean getmStartButtonStatus() {
        return mStartButtonStatus;
    }

    public void setmStartButtonStatus(Boolean mStartButtonStatus) {
        this.mStartButtonStatus = mStartButtonStatus;
    }
}
