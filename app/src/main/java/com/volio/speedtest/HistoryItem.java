package com.volio.speedtest;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class HistoryItem implements Serializable {
    private String mUploadRate;
    private String mDownloadRate;
    private String mPingRate;
    private String mNameNetwork;
    private int mTypeNetwork;
    private String mDateTime;
    private String mLatitude;
    private String mLongitude;
    private String mExternalIp;
    private String mInternalIp;
    private String mDowloadData;
    private String mUploadData;

    public HistoryItem(String mUploadRate, String mDownloadRate, String mPingRate, String mNameNetwork, int mTypeNetwork, String mDateTime, String mLatitude, String mLongitude, String mExternalIp, String mInternalIp,String mUploadData,String mDowloadData) {
        this.mUploadRate = mUploadRate;
        this.mDownloadRate = mDownloadRate;
        this.mPingRate = mPingRate;
        this.mNameNetwork = mNameNetwork;
        this.mTypeNetwork = mTypeNetwork;
        this.mDateTime = mDateTime;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
        this.mExternalIp = mExternalIp;
        this.mInternalIp = mInternalIp;
        this.mDowloadData=mDowloadData;
        this.mUploadData=mUploadData;
    }

    public String getmDowloadData() {
        return mDowloadData;
    }

    public void setmDowloadData(String mDowloadData) {
        this.mDowloadData = mDowloadData;
    }

    public String getmUploadData() {
        return mUploadData;
    }

    public void setmUploadData(String mUploadData) {
        this.mUploadData = mUploadData;
    }

    public String getmUploadRate() {
        return mUploadRate;
    }

    public void setmUploadRate(String mUploadRate) {
        this.mUploadRate = mUploadRate;
    }

    public String getmDownloadRate() {
        return mDownloadRate;
    }

    public void setmDownloadRate(String mDownloadRate) {
        this.mDownloadRate = mDownloadRate;
    }

    public String getmPingRate() {
        return mPingRate;
    }

    public void setmPingRate(String mPingRate) {
        this.mPingRate = mPingRate;
    }

    public String getmNameNetwork() {
        return mNameNetwork;
    }

    public void setmNameNetwork(String mNameNetwork) {
        this.mNameNetwork = mNameNetwork;
    }

    public int getmTypeNetwork() {
        return mTypeNetwork;
    }

    public void setmTypeNetwork(int mTypeNetwork) {
        this.mTypeNetwork = mTypeNetwork;
    }

    public String getmDateTime() {
        return mDateTime;
    }

    public void setmDateTime(String mDateTime) {
        this.mDateTime = mDateTime;
    }

    public String getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getmExternalIp() {
        return mExternalIp;
    }

    public void setmExternalIp(String mExternalIp) {
        this.mExternalIp = mExternalIp;
    }

    public String getmInternalIp() {
        return mInternalIp;
    }

    public void setmInternalIp(String mInternalIp) {
        this.mInternalIp = mInternalIp;
    }
}
