package com.volio.speedtest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import com.volio.speedtest.GetSpeedTestHostsHandler;
import com.volio.speedtest.HistoryItem;
import com.volio.speedtest.network.Connectivity;
import com.volio.speedtest.test.HttpDownloadTest;
import com.volio.speedtest.test.HttpUploadTest;
import com.volio.speedtest.test.PingTest;
import com.volio.speedtest.viewmodel.SharedViewModel;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.lifecycle.ViewModelProviders;

public class AsynchSpeedTest extends AsyncTask<Void, Integer, Void> {
    private GetSpeedTestHostsHandler getSpeedTestHostsHandler;
    private HttpUploadTest httpUploadTest;
    private HttpDownloadTest httpDownloadTest;
    private PingTest pingTest;
    private HttpDownloadTest downloadTest;
    private HttpUploadTest uploadTest;
    private String mStartButtonString;
    private Boolean mStartButtonStatus = true;
    private String mToastNotificatonString;
    private int mStartButtonSize = 12;
    private Boolean allFinished = false;
    private double selfLat = 0.0;
    private double selfLon = 0.0;
    private final DecimalFormat dec = new DecimalFormat("#.##");
    private String mPingTextViewString = "0 ms";
    private String mUploadTextViewString = "0 Mbps";
    private String mDownloadTextViewString = "0 Mbps";
    private static float position = 0;
    private static float lastPosition = 0;
    private String mDateTimeString;
    private String mExternalIp;
    private String mInternalIp;
    private static final String FILECACHE = "speed_test_data.dat";
    private List<HistoryItem> mListHistoryItems = new ArrayList<>();
    Context context;

    public void restartSpeedTestHostsHandler() {
        getSpeedTestHostsHandler = new GetSpeedTestHostsHandler();
        getSpeedTestHostsHandler.start();
    }

    String uploadAddr;
    List<String> info;
    double distance;
    int typeCountinue = 0;

    public AsynchSpeedTest(Context context) {
        this.context = context;
        getSpeedTestHostsHandler = null;
    }

    @Override
    protected void onPreExecute() {

        //this method will be running on UI thread
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected Void doInBackground(Void... params) {
        restartSpeedTestHostsHandler();
        int timeCount = 600;
        //0
        typeCountinue = 0;
        publishProgress(typeCountinue);
        while (!getSpeedTestHostsHandler.isFinished()) {
            timeCount--;
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (timeCount <= 0) {
                //1
                typeCountinue = 1;
                publishProgress(typeCountinue);
                getSpeedTestHostsHandler = null;
                return null;
            }

        }
        HashMap<Integer, String> mapKey = getSpeedTestHostsHandler.getMapKey();
        HashMap<Integer, List<String>> mapValue = getSpeedTestHostsHandler.getMapValue();
        if (selfLat == 0.0) {
            selfLat = getSpeedTestHostsHandler.getSelfLat();
            selfLon = getSpeedTestHostsHandler.getSelfLon();

        }
        double dist = 0.0;
        int findServerIndex = 0;
        double tmp = 19349458;
        //calculate distance
        for (int index : mapKey.keySet()) {

            Location source = new Location("Source");
            source.setLatitude(selfLat);
            source.setLongitude(selfLon);

            List<String> ls = mapValue.get(index);
            Location dest = new Location("Dest");

            dest.setLatitude(Double.parseDouble(ls.get(0)));
            dest.setLongitude(Double.parseDouble(ls.get(1)));
            distance = source.distanceTo(dest);
            if (tmp > distance) {
                tmp = distance;
                dist = distance;
                findServerIndex = index;
            }
        }
        uploadAddr = mapKey.get(findServerIndex);
        info = mapValue.get(findServerIndex);
        distance = dist;
        //2
        typeCountinue = 2;
        publishProgress(typeCountinue);
        typeCountinue = 3;
        //3
        publishProgress(typeCountinue);
        final List<Double> pingRateList = new ArrayList<>();
        final List<Double> downloadRateList = new ArrayList<>();
        final List<Double> uploadRateList = new ArrayList<>();
        Boolean pingTestStarted = false;
        Boolean pingTestFinished = false;
        Boolean downloadTestStarted = false;
        Boolean downloadTestFinished = false;
        Boolean uploadTestStarted = false;
        Boolean uploadTestFinished = false;
        pingTest = new PingTest(info.get(6).replace(":8080", ""), 6);
        downloadTest = new HttpDownloadTest(uploadAddr.replace(uploadAddr.split("/")[uploadAddr.split("/").length - 1], ""));
        uploadTest = new HttpUploadTest(uploadAddr);
        while (true) {
            if (!pingTestStarted) {
                pingTest.start();
                pingTestStarted = true;
            }
            if (pingTestFinished && !downloadTestStarted) {
                downloadTest.start();
                downloadTestStarted = true;
            }
            if (downloadTestFinished && !uploadTestStarted) {
                uploadTest.start();
                uploadTestStarted = true;
            }
            //Ping Test
            if (pingTestFinished) {
                //Failure
                if (pingTest.getAvgRtt() == 0) {
                    System.out.println("Ping error...");
                } else {
                    //4===================
                    typeCountinue = 4;
                    publishProgress(typeCountinue);

                }
            } else {
                pingRateList.add(pingTest.getInstantRtt());
                //5====================
                typeCountinue = 5;
                publishProgress(typeCountinue);

            }


            //Download Test
            if (pingTestFinished) {
                if (downloadTestFinished) {
                    //Failure
                    if (downloadTest.getFinalDownloadRate() == 0) {
                        System.out.println("Download error...");
                    } else {
                        //6=====================
                        typeCountinue = 6;
                        publishProgress(typeCountinue);

                    }
                } else {
                    //Calc position
                    //7================
                    double downloadRate = downloadTest.getInstantDownloadRate();
                    position = getPositionByRate(downloadRate);
                    typeCountinue = 7;
                    publishProgress(typeCountinue);
                    lastPosition=position;
                }
            }


            //Upload Test
            if (downloadTestFinished) {
                if (uploadTestFinished) {
                    //Failure
                    if (uploadTest.getFinalUploadRate() == 0) {
                        System.out.println("Upload error...");
                    } else {
                        //8======================
                        typeCountinue = 8;
                        publishProgress(typeCountinue);

                    }
                } else {
                    //Calc position
                    //9==========================
                    double uploadRate = uploadTest.getInstantUploadRate();
                    uploadRateList.add(uploadRate);
                    position = getPositionByRate(uploadRate);
                    typeCountinue = 9;
                    publishProgress(typeCountinue);
                    lastPosition=position;
                }
            }

            //Test bitti
            if (pingTestFinished && downloadTestFinished && uploadTest.isFinished()) {
                Log.i("BackgroundTestService", "do In background");
                break;
            }

            if (pingTest.isFinished()) {
                pingTestFinished = true;
            }
            if (downloadTest.isFinished()) {
                downloadTestFinished = true;
            }
            if (uploadTest.isFinished()) {
                uploadTestFinished = true;
            }

            if (pingTestStarted && !pingTestFinished) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                }
            } else {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
        }
        //10=================
        typeCountinue=10;
        publishProgress(typeCountinue);
         mUploadString = dec.format(uploadTest.getFinalUploadRate()) + "Mbps";
         mDownloadString = dec.format(downloadTest.getFinalDownloadRate()) + "Mbps";
         mPingString = pingTest.getAvgRtt() + "Mbps";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date();
        mDateTimeString = formatter.format(date);

        try (
                final DatagramSocket socket = new DatagramSocket())

        {
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            mExternalIp = socket.getLocalAddress().getHostAddress();
        } catch (
                SocketException e)

        {
            e.printStackTrace();
        } catch (
                UnknownHostException e)

        {
            e.printStackTrace();
        }

        mInternalIp = getSpeedTestHostsHandler.getmIP();
        return null;
    }

    private String mUploadString;
    private  String mPingString;
    private String mDownloadString;

    @Override
    protected void onProgressUpdate(Integer... values) {
        Intent local = new Intent();
        local.setAction("volio");
        if (values[0] == 1) {
            local.putExtra("typeCountinue", 1);
        } else if (values[0] == 2) {
            local.putExtra("info", info.get(2));
            local.putExtra("distance", distance);
            local.putExtra("selfLat", selfLat);
            local.putExtra("selfLon", selfLon);
            local.putExtra("typeCountinue", 2);
        } else if (values[0] == 0) {
            local.putExtra("typeCountinue", 0);
        } else if (values[0] == 4) {
            local.putExtra("pingTestAvg", pingTest.getAvgRtt());
            local.putExtra("typeCountinue", 4);
        } else if (values[0] == 3) {
            local.putExtra("typeCountinue", 3);
        } else if (values[0] == 5) {
            local.putExtra("pingTestInstant", pingTest.getInstantRtt());
            local.putExtra("typeCountinue", 5);
        } else if (values[0] == 6) {
            local.putExtra("downloadTestFinal", downloadTest.getFinalDownloadRate());
            local.putExtra("typeCountinue", 6);
        } else if (values[0] == 7) {
            local.putExtra("position",position);
            local.putExtra("lastposition",lastPosition);
            local.putExtra("downloadTestInstant", downloadTest.getInstantDownloadRate());
            local.putExtra("typeCountinue", 7);
        } else if (values[0] == 8) {
            local.putExtra("uploadTestFinal", uploadTest.getFinalUploadRate());
            local.putExtra("typeCountinue", 8);
        } else if (values[0] == 9) {
            local.putExtra("position",position);
            local.putExtra("lastposition",lastPosition);
            local.putExtra("uploadTestInstant", uploadTest.getInstantUploadRate());
            local.putExtra("typeCountinue", 9);
        }
        else if (values[0] == 10) {
            local.putExtra("dateTimeString",mDateTimeString);
            local.putExtra("externalIP",mExternalIp);
            local.putExtra("internalIP",mInternalIp);
            local.putExtra("typeCountinue", 10);
        }

        context.sendBroadcast(local);
    }
    public float getPositionByRate(double rate) {
        if (rate >= 0 && rate <= 120) {
            return (float) rate * 2;
        }
        return 0;
    }
    String nameNetWork;
    int typeNetWork;
    @Override
    protected void onPostExecute(Void result) {
        //this method will be running on UI thread
        cancel(true);

    }

}
