package com.volio.speedtest.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;

import com.volio.speedtest.AsynchSpeedTest;

public class BackgroundTestService2 extends Service {
    private AsynchSpeedTest asyncCaller;
    private final IBinder binder = new LocalBackgroundTestBinder2();
    public class LocalBackgroundTestBinder2 extends Binder {
        public BackgroundTestService2 getService() {
            return BackgroundTestService2.this;
        }
    }
    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.i("BackgroundTestService","onRebind");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i("BackgroundTestService","onUnbind");
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("BackgroundTestService","onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("BackgroundTestService","onDestroy");
    }

    @Override
    public IBinder onBind(Intent intent) {

        mMessenger=new Messenger(new IncomingHandle(this));
        Toast.makeText(getApplicationContext(), "binding", Toast.LENGTH_SHORT).show();
        return mMessenger.getBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        asyncCaller= (AsynchSpeedTest) new AsynchSpeedTest(getApplicationContext()).execute();
        Log.i("BackgroundTestService","onStartCommand");
        return START_NOT_STICKY;
    }
    public int getNumber(){
        return 7;
    }
    static final int MSG_SAY_HELLO = 1;
    static class IncomingHandle extends Handler{
        private Context applicationContext;
        IncomingHandle(Context context){
            applicationContext=context.getApplicationContext();
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case MSG_SAY_HELLO:
                    Toast.makeText(applicationContext,"hello",Toast.LENGTH_LONG).show();
            }
            super.handleMessage(msg);
        }
    }
    Messenger mMessenger;


}
