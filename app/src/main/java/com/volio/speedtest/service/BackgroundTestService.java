package com.volio.speedtest.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.volio.speedtest.ThreadSpeedTest;
import com.volio.speedtest.test.HttpDownloadTest;
import com.volio.speedtest.test.HttpUploadTest;
import com.volio.speedtest.test.PingTest;

import androidx.annotation.Nullable;

public class BackgroundTestService extends Service {
    private final IBinder binder = new LocalBackgroundTestBinder();
    private String UrlUploadDownload;
    private String UrlPing;
    private PingTest pingTest=null;
    private HttpDownloadTest downloadTest=null;
    private HttpUploadTest uploadTest=null ;
    private Thread thread1;
    public static Thread performOnBackgroundThread(final Runnable runnable) {
        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } finally {

                }
            }
        };
        t.start();
        return t;
    }
    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.i("BackgroundTestService","onRebind");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i("BackgroundTestService","onUnbind");
        return true;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i("BackgroundTestService","onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("BackgroundTestService","onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i("BackgroundTestService","onBind");
        return binder;
    }

    public void getUploadAddr(String UrlUploadDownload, String UrlPing) {
        this.UrlUploadDownload = UrlUploadDownload;
        this.UrlPing = UrlPing;
        pingTest = new PingTest(UrlPing.replace(":8080", ""), 6);
        downloadTest = new HttpDownloadTest(UrlUploadDownload.replace(UrlUploadDownload.split("/")[UrlUploadDownload.split("/").length - 1], ""));
        uploadTest = new HttpUploadTest(UrlUploadDownload);
    }

    public class LocalBackgroundTestBinder extends Binder {
        public BackgroundTestService getService() {
            return BackgroundTestService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i("BackgroundTestService","onStartCommand");
        return START_NOT_STICKY;
    }

    public PingTest getPingTest() {
        return pingTest;
    }

    public HttpDownloadTest getDownloadTest() {
        return downloadTest;
    }

    public HttpUploadTest getUploadTest() {
        return uploadTest;
    }
    public String Url(){
        return UrlUploadDownload+UrlPing;
    }


}
