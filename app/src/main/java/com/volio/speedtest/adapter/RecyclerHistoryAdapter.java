package com.volio.speedtest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.volio.speedtest.HistoryItem;
import com.volio.speedtest.R;
import com.volio.speedtest.callback.ClickHistoryItemCallBack;
import com.volio.speedtest.callback.SpeedTestCallBack;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerHistoryAdapter extends RecyclerView.Adapter<RecyclerHistoryAdapter.ViewHolder> {
    private List<HistoryItem> mListHistoryArray;
    private Context mContext;
    private ClickHistoryItemCallBack mClickHistoryItemCallBack;
    public void setNewHistoryItem(HistoryItem historyItem){
        mListHistoryArray.add(historyItem);
    }
    public RecyclerHistoryAdapter(List<HistoryItem> mListHistoryArray) {
        this.mListHistoryArray = mListHistoryArray;
    }
    public void setmClickHistoryItemCallBack(ClickHistoryItemCallBack clickHistoryItemCallBack){
        this.mClickHistoryItemCallBack=clickHistoryItemCallBack;
    }

    public List<HistoryItem> getmListHistoryArray() {
        return mListHistoryArray;
    }

    public void setmListHistoryArray(List<HistoryItem> mListHistoryArray) {
        this.mListHistoryArray=null;
        this.mListHistoryArray=new ArrayList<>();
        this.mListHistoryArray.addAll(mListHistoryArray);
        notifyDataSetChanged();
    }
    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
        TextView mUploadTextView = holder.mUploadTextView;
        TextView mDownloadTextView = holder.mDownloadTextView;
        TextView mPingTextView = holder.mPingTextView;
        TextView mNameNetworkTextView = holder.mNameNetworkTextView;
        ImageView mIconNetworkImageView = holder.mIconNetworkImageView;
        TextView mDateTimeTextView = holder.mDateTimeTextView;
        HistoryItem historyItem = mListHistoryArray.get(position);
        mUploadTextView.setText("" + historyItem.getmUploadRate());
        mDownloadTextView.setText("" + historyItem.getmDownloadRate());
        mPingTextView.setText("" + historyItem.getmPingRate());
        mNameNetworkTextView.setText("" + historyItem.getmNameNetwork());
        if (historyItem.getmTypeNetwork() == 1) {
            mIconNetworkImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.item_wifi));
        }
        else
        {
            mIconNetworkImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.item_3g));
        }
        mDateTimeTextView.setText(historyItem.getmDateTime());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickHistoryItemCallBack.onClickItem(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListHistoryArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mUploadTextView;
        private TextView mDownloadTextView;
        private TextView mPingTextView;
        private TextView mNameNetworkTextView;
        private ImageView mIconNetworkImageView;
        private TextView mDateTimeTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mUploadTextView = itemView.findViewById(R.id.mUploadTextView);
            mDownloadTextView = itemView.findViewById(R.id.mDownloadTextView);
            mPingTextView = itemView.findViewById(R.id.mPingTextView);
            mIconNetworkImageView = itemView.findViewById(R.id.mNetworkImageView);
            mNameNetworkTextView = itemView.findViewById(R.id.mNameNetworkTextView);
            mDateTimeTextView = itemView.findViewById(R.id.mDateTimeTextView);
        }
    }
    private int lastPosition = -1;
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
