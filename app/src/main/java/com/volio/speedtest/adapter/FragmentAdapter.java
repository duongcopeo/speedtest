package com.volio.speedtest.adapter;


import android.util.Log;

import com.volio.speedtest.HistoryItem;
import com.volio.speedtest.callback.SpeedTestCallBack;
import com.volio.speedtest.fragment.FragmentHistory;
import com.volio.speedtest.fragment.FragmentHistoryDetail;
import com.volio.speedtest.fragment.FragmentSpeed;
import com.volio.speedtest.fragment.FragmentSpeedDetail;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class FragmentAdapter extends FragmentStatePagerAdapter {
    private List<String> arrTitle = new ArrayList<>();
    private FragmentManager fragmentManager;
    private Fragment mFragmentAtPostition0;
    private Fragment mFragmentAtPostition1;
    private static final int NUMBER_FRAGMENT = 2;

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
        arrTitle.add("Speed");
        arrTitle.add("History");
        this.fragmentManager = fm;
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0) {
            if (mFragmentAtPostition0 == null) {
                mFragmentAtPostition0 = FragmentSpeed.newInstance();

            }
            return mFragmentAtPostition0;
        } else if (i == 1) {
            if (mFragmentAtPostition1 == null) {
                mFragmentAtPostition1=FragmentHistory.newInstance();
                regressionOfFragmentCallBackHaveHistory(1);
            }
            return mFragmentAtPostition1;
        }
        return null;
    }
    public void regressionOfFragmentCallBackHaveHistory(int type){
        if(type==1){
            Log.i("FragmentAdapter","type1");
            ((FragmentHistory) mFragmentAtPostition1).setFragmentHistoryListener(new FragmentHistoryListener() {
                @Override
                public void onSwitchToNextFragment(HistoryItem historyItem) {
                    fragmentManager.beginTransaction().remove(mFragmentAtPostition1).commit();
                    mFragmentAtPostition1 = FragmentHistoryDetail.newInstance();
                    ((FragmentHistoryDetail) mFragmentAtPostition1).setHistoryItem(historyItem);
                    fragmentManager.beginTransaction().remove(mFragmentAtPostition0).commit();
                    mFragmentAtPostition0 = FragmentSpeedDetail.newInstance();
                    ((FragmentSpeedDetail) mFragmentAtPostition0).setHistoryItem(historyItem);
                    regressionOfFragmentCallBackHaveHistory(2);
                    FragmentAdapter.this.notifyDataSetChanged();
                }
            });
        }
        if(type==2){
            Log.i("FragmentAdapter","type2");
            ((FragmentSpeedDetail) mFragmentAtPostition0).setFragmentHistoryListenerAgain(new FragmentHistoryListenerAgain() {
                @Override
                public void onSwitchToNextFragment2() {
                    fragmentManager.beginTransaction().remove(mFragmentAtPostition1).commit();
                    mFragmentAtPostition1 = FragmentHistory.newInstance();
                    regressionOfFragmentCallBackHaveHistory(1);
                    fragmentManager.beginTransaction().remove(mFragmentAtPostition0).commit();
                    mFragmentAtPostition0 = FragmentSpeed.newInstance();
                    FragmentAdapter.this.notifyDataSetChanged();
                }
            });
        }
    }
    @Override
    public int getCount() {
        return NUMBER_FRAGMENT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arrTitle.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof FragmentHistory && mFragmentAtPostition1 instanceof FragmentHistoryDetail)
            return POSITION_NONE;
        if (object instanceof FragmentHistoryDetail && mFragmentAtPostition1 instanceof FragmentHistory)
            return POSITION_NONE;
        if (object instanceof FragmentSpeed && mFragmentAtPostition0 instanceof FragmentSpeedDetail)
            return POSITION_NONE;
        if (object instanceof FragmentSpeedDetail && mFragmentAtPostition0 instanceof FragmentSpeed)
            return POSITION_NONE;
        return POSITION_UNCHANGED;
    }

    public interface FragmentHistoryListener {
        void onSwitchToNextFragment(HistoryItem historyItem);
    }

    public interface FragmentHistoryListenerAgain {
        void onSwitchToNextFragment2();
    }
}
