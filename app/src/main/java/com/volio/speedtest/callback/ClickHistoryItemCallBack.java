package com.volio.speedtest.callback;

import android.view.View;

public interface ClickHistoryItemCallBack  {
    void onClickItem(int position);
}
