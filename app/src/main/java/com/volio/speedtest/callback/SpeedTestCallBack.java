package com.volio.speedtest.callback;

import android.view.View;

import com.volio.speedtest.ThreadSpeedTest;
import com.volio.speedtest.service.BackgroundTestService;
import com.volio.speedtest.service.BackgroundTestService2;

public interface SpeedTestCallBack  {
    public BackgroundTestService2 transportBackgroundTestService2();
    public BackgroundTestService transportBackgroundTestService();
    public boolean getMBoundStatus();
    public void startBackgroundTestService2();
    public void bindService();
    public void unbindService();
}
