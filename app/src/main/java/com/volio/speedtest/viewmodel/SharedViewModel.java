package com.volio.speedtest.viewmodel;


import com.volio.speedtest.HistoryItem;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class SharedViewModel extends ViewModel {
    private MutableLiveData<HistoryItem> historyItem=new MutableLiveData<>();
    public void setHistoryItem(HistoryItem historyItem){
        this.historyItem.setValue(historyItem);
    }
    public LiveData<HistoryItem> getHistoryItem(){
        return historyItem;
    }
}
