//package com.volio.speedtest.fragment;
//
//import android.Manifest;
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.Service;
//import android.content.ComponentName;
//import android.content.Context;
//import android.content.Intent;
//import android.content.ServiceConnection;
//import android.content.pm.PackageManager;
//import android.graphics.Color;
//import android.location.Location;
//import android.net.Uri;
//import android.net.wifi.WifiInfo;
//import android.net.wifi.WifiManager;
//import android.os.Bundle;
//import android.os.IBinder;
//import android.provider.Settings;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.view.animation.LinearInterpolator;
//import android.view.animation.RotateAnimation;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.location.FusedLocationProviderClient;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.android.material.snackbar.Snackbar;
//import com.volio.speedtest.BuildConfig;
//import com.volio.speedtest.GetSpeedTestHostsHandler;
//import com.volio.speedtest.HistoryItem;
//import com.volio.speedtest.Main2Activity;
//import com.volio.speedtest.R;
//import com.volio.speedtest.ThreadSpeedTest;
//import com.volio.speedtest.callback.SpeedTestCallBack;
//import com.volio.speedtest.network.Connectivity;
//import com.volio.speedtest.service.BackgroundTestService;
//import com.volio.speedtest.service.BackgroundTestService2;
//import com.volio.speedtest.test.HttpDownloadTest;
//import com.volio.speedtest.test.HttpUploadTest;
//import com.volio.speedtest.test.PingTest;
//import com.volio.speedtest.viewmodel.SharedViewModel;
//
//import org.achartengine.ChartFactory;
//import org.achartengine.GraphicalView;
//import org.achartengine.model.XYMultipleSeriesDataset;
//import org.achartengine.model.XYSeries;
//import org.achartengine.renderer.XYMultipleSeriesRenderer;
//import org.achartengine.renderer.XYSeriesRenderer;
//
//import java.net.DatagramSocket;
//import java.net.InetAddress;
//import java.net.SocketException;
//import java.net.UnknownHostException;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.LinkedHashMap;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.core.app.ActivityCompat;
//import androidx.fragment.app.Fragment;
//import androidx.interpolator.view.animation.FastOutLinearInInterpolator;
//import androidx.lifecycle.ViewModelProviders;
//
//public class FragmentSpeed2 extends Fragment {
//    private View mFragmentView;
//    private Activity mActivity;
//    private final static String TAG = Main2Activity.class.getSimpleName();
//    private View mViewMainActivity;
//    private Button mStartButton;
//    private ImageView mIconNetworkImageView;
//    private TextView mNameNetworkTextView;
//    private LinearLayout mUploadLinearLayout;
//    private LinearLayout mDownloadLinearLayout;
//    private Context mContext;
//    private static float position = 0;
//    private static float lastPosition = 0;
//    private GetSpeedTestHostsHandler getSpeedTestHostsHandler = null;
//    private HashSet<String> tempBlackList;
//    private double selfLat = 0.0;
//    private double selfLon = 0.0;
//    private String mUploadString;
//    private String mDownloadString;
//    private String mPingString;
//    private String mDateTimeString;
//    private String mExternalIp;
//    private String mInternalIp;
//
//    private BackgroundTestService2 mService = null;
//    final DecimalFormat dec = new DecimalFormat("#.##");
//    private boolean mBound = false;
//    private SpeedTestCallBack speedTestCallBack;
//    private ThreadSpeedTest mThreadSpeedTest;
//    private RelativeLayout mClockWise;
//    private TextView mPingTextView;
//    private TextView mUploadTextView;
//    private TextView mDownloadTextView;
//    private TextView mDownloadTextView2;
//    private TextView mUploadTextView2;
//    private RotateAnimation rotate;
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        speedTestCallBack.unbindService();
//    }
//
//    private Intent intentFragmentSpeed;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
//        mFragmentView = inflater.inflate(R.layout.fragment_speed, container, false);
//        intentFragmentSpeed = new Intent(mActivity, BackgroundTestService2.class);
//        mClockWise = mFragmentView.findViewById(R.id.barImageView2);
//        mPingTextView = mFragmentView.findViewById(R.id.pingTextView);
//        mUploadTextView = mFragmentView.findViewById(R.id.uploadTextView);
//        mDownloadTextView = mFragmentView.findViewById(R.id.downloadTextView);
//        mDownloadTextView2 = mFragmentView.findViewById(R.id.downloadTextView2);
//        mUploadTextView2 = mFragmentView.findViewById(R.id.uploadTextView2);
//        mViewMainActivity = container;
//        mFusedLocationProviderClient = new FusedLocationProviderClient(mActivity);
//        mUploadLinearLayout = mFragmentView.findViewById(R.id.UploadLinearLayout);
//        mDownloadLinearLayout = mFragmentView.findViewById(R.id.DownloadLinearLayout);
//        mIconNetworkImageView = mFragmentView.findViewById(R.id.ic_network);
//        mNameNetworkTextView = mFragmentView.findViewById(R.id.name_network);
//        mStartButton = mFragmentView.findViewById(R.id.startButton2);
//        mService = speedTestCallBack.transportBackgroundTestService2();
//        mThreadSpeedTest=new ThreadSpeedTest();
//        OnBtnClickListener onBtnClickListener = new OnBtnClickListener();
//        mStartButton.setOnClickListener(onBtnClickListener);
//        return mFragmentView;
//    }
//
//    public void setSpeedTestCallBack(Main2Activity activity) {
//        speedTestCallBack = activity;
//    }
//
//    private SharedViewModel sharedViewModel;
//
//    private void addHistoryItem() {
//        HistoryItem historyItem = new HistoryItem(mThreadSpeedTest.getmUploadTextViewString(),
//                mThreadSpeedTest.getmDownloadTextViewString(),
//                mThreadSpeedTest.getmPingTextViewString(),
//                nameNetWork,
//                typeNetWork,
//                mThreadSpeedTest.getmDateTimeString(),
//                "" + mThreadSpeedTest.getSelfLat(),
//                "" + mThreadSpeedTest.getSelfLon(),
//                mThreadSpeedTest.getmExternalIp(),
//                mThreadSpeedTest.getmInternalIp(),"","");
//        sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
//        sharedViewModel.setHistoryItem(historyItem);
//    }
//
//
//    private boolean checkPermissions(String accessCoarseLocation) {
//        int permissionState = ActivityCompat.checkSelfPermission(mContext, accessCoarseLocation);
//        return permissionState == PackageManager.PERMISSION_GRANTED;
//    }
//
//    //========================================BEHAVIOUR METHOD TO GET SPEED TEST==============//
//    public void restartSpeedTestHostsHandler() {
//        getSpeedTestHostsHandler = new GetSpeedTestHostsHandler();
//        getSpeedTestHostsHandler.start();
//    }
//
//
//    //FROM_HERE-----------------------MPS_TO_ANPHA-----------------------------------------
//    public float getPositionByRate(double rate) {
//        if (rate >= 0 && rate <= 120) {
//            return (float) rate * 2;
//        }
//        return 0;
//    }
//
//    //FROM_HERE-----------------------DEFAULT-----------------------------------------
//    public void showSnackBar(final String txt) {
//        View v = mViewMainActivity.findViewById(R.id.main_activity_container2);
//        if (v != null) {
//            Snackbar.make(v, txt, Snackbar.LENGTH_LONG).show();
//        }
//    }
//
//    public void showSnackBar(final int mainTextString, final int textActionString, View.OnClickListener listener) {
//        Snackbar.make(mViewMainActivity.findViewById(R.id.main_activity_container2),
//                getString(mainTextString),
//                Snackbar.LENGTH_INDEFINITE).setAction(getString(textActionString), listener).show();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (!checkPermissions(Manifest.permission.ACCESS_COARSE_LOCATION)) {
//            requestPermission();
//        } else {
//            getLastLotation();
//        }
//        restartSpeedTestHostsHandler();
//    }
//
//    private void requestPermission() {
//        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION);
//        if (shouldProvideRationale) {
//            Log.i(TAG, "Displaying permission to provide additional context");
//            showSnackBar(R.string.permission_rationale, android.R.string.ok, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    startRequestPermissionResult();
//                }
//            });
//        } else {
//            Log.i(TAG, "Requesting permission");
//            startRequestPermissionResult();
//        }
//    }
//
//    private int REQUEST_PERMISSION_CODE_ALL = 32;
//
//    private void startRequestPermissionResult() {
//        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSION_CODE_ALL);
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        Log.i(TAG, "onRequestPermissionsResult");
//        if (grantResults.length <= 0) {
//            Log.i(TAG, "User interaction was cancelled.");
//        } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            getLastLotation();
//        } else {
//            showSnackBar(R.string.permission_denied_need, R.string.settings, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent();
//                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                    Uri uri = Uri.fromParts("pakage", BuildConfig.APPLICATION_ID, null);
//                    intent.setData(uri);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
//            });
//        }
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//
//    private FusedLocationProviderClient mFusedLocationProviderClient;
//
//    @SuppressLint("MissingPermission")
//    private void getLastLotation() {
//        mFusedLocationProviderClient.getLastLocation()
//                .addOnCompleteListener(new OnCompleteListener<Location>() {
//                    @Override
//                    public void onComplete(Task<Location> task) {
//                        if (task.getResult() != null && task.isSuccessful()) {
//                            selfLat = task.getResult().getLatitude();
//                            selfLon = task.getResult().getLongitude();
//                        } else {
//                            Log.e(TAG, "get last lotation exception");
//                            showSnackBar(getString(R.string.no_lotation_detected));
//                        }
//                    }
//                });
//
//    }
//
//    private OnFragmentInteractionListener mListener;
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        mContext = context;
//        mActivity = (Activity) context;
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//        }
//    }
//
//    public static HashMap<Integer, Float> sortByValue(HashMap<Integer, Float> hm) {
//        // Create a list from elements of HashMap
//        List<Map.Entry<Integer, Float>> list =
//                new LinkedList<Map.Entry<Integer, Float>>(hm.entrySet());
//
//        // Sort the list
//        Collections.sort(list, new Comparator<Map.Entry<Integer, Float>>() {
//            public int compare(Map.Entry<Integer, Float> o1,
//                               Map.Entry<Integer, Float> o2) {
//                return (o1.getValue()).compareTo(o2.getValue());
//            }
//        });
//
//        // put data from sorted list to hashmap
//        HashMap<Integer, Float> temp = new LinkedHashMap<Integer, Float>();
//        for (Map.Entry<Integer, Float> aa : list) {
//            temp.put(aa.getKey(), aa.getValue());
//        }
//        return temp;
//    }
//
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
//
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    private String nameNetWork;
//    private int typeNetWork;
//    Connectivity connectivity;
//
//    private class OnBtnClickListener implements View.OnClickListener {
//        @Override
//        public void onClick(View v) {
//            //BackgroundTestService.performOnBackgroundThread(runnable2);
//
//        }
//    }
//
//        Runnable runnable2 = new Runnable() {
//        @Override
//        public void run() {
//
//            mActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    connectivity = new Connectivity(mContext);
//                    connectivity.detectInformationNetwork();
//                    nameNetWork = connectivity.getNameNetWork();
//                    typeNetWork = connectivity.getTypeNetWork();
//                }
//            });
//            mActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    while (!mThreadSpeedTest.getAllFinished()) {
//                        mStartButton.setEnabled(mThreadSpeedTest.getmStartButtonStatus());
//                        mStartButton.setText(mThreadSpeedTest.getmStartButtonString());
//                        mStartButton.setTextSize(mThreadSpeedTest.getmStartButtonSize());
//                    }
//                }
//            });
//            mActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    while (!mThreadSpeedTest.getAllFinished()) {
//                        Toast.makeText(mContext, mThreadSpeedTest.getmToastNotificatonString(), Toast.LENGTH_SHORT).show();
//
//                    }
//                }
//            });
//            /*
//            Caution: A service runs in the same process as the application in which
//             it is declared and in the main thread of that application by default.
//             If your service performs intensive or blocking operations while the user interacts
//              with an activity from the same application,
//              the service slows down activity performance. To avoid impacting application performance,
//              start a new thread inside the service
//              Service
//              This is the base class for all services. When you extend this class,
//              it's important to create a new thread in which the service can complete all of its work;
//               the service uses your application's main thread by default,
//                which can slow the performance of any activity that your application is running.
//             */
//
//
//            mActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    while (!mThreadSpeedTest.getAllFinished())
//                        mStartButton.setText(mThreadSpeedTest.getmStartButtonString());
//                }
//            });
//            XYSeriesRenderer pingRenderer = new XYSeriesRenderer();
//            XYSeriesRenderer.FillOutsideLine pingFill = new XYSeriesRenderer.FillOutsideLine(XYSeriesRenderer.FillOutsideLine.Type.BOUNDS_ALL);
//            pingFill.setColor(Color.parseColor("#4d5a6a"));
//            pingRenderer.addFillOutsideLine(pingFill);
//            pingRenderer.setDisplayChartValues(false);
//            pingRenderer.setShowLegendItem(false);
//            pingRenderer.setColor(Color.parseColor("#4d5a6a"));
//            pingRenderer.setLineWidth(5);
//            final XYMultipleSeriesRenderer multiPingRenderer = new XYMultipleSeriesRenderer();
//            multiPingRenderer.setXLabels(0);
//            multiPingRenderer.setYLabels(0);
//            multiPingRenderer.setZoomEnabled(false);
//            multiPingRenderer.setXAxisColor(Color.parseColor("#647488"));
//            multiPingRenderer.setYAxisColor(Color.parseColor("#2F3C4C"));
//            multiPingRenderer.setPanEnabled(true, true);
//            multiPingRenderer.setZoomButtonsVisible(false);
//            multiPingRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));
//            multiPingRenderer.addSeriesRenderer(pingRenderer);
//
//            //Init Download graphic
//            XYSeriesRenderer downloadRenderer = new XYSeriesRenderer();
//            XYSeriesRenderer.FillOutsideLine downloadFill = new XYSeriesRenderer.FillOutsideLine(XYSeriesRenderer.FillOutsideLine.Type.BOUNDS_ALL);
//            downloadFill.setColor(Color.parseColor("#4d5a6a"));
//            downloadRenderer.addFillOutsideLine(downloadFill);
//            downloadRenderer.setDisplayChartValues(false);
//            downloadRenderer.setColor(Color.parseColor("#4d5a6a"));
//            downloadRenderer.setShowLegendItem(false);
//            downloadRenderer.setLineWidth(5);
//            final XYMultipleSeriesRenderer multiDownloadRenderer = new XYMultipleSeriesRenderer();
//            multiDownloadRenderer.setXLabels(0);
//            multiDownloadRenderer.setYLabels(0);
//            multiDownloadRenderer.setZoomEnabled(false);
//            multiDownloadRenderer.setXAxisColor(Color.parseColor("#647488"));
//            multiDownloadRenderer.setYAxisColor(Color.parseColor("#2F3C4C"));
//            multiDownloadRenderer.setPanEnabled(false, false);
//            multiDownloadRenderer.setZoomButtonsVisible(false);
//            multiDownloadRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));
//            multiDownloadRenderer.addSeriesRenderer(downloadRenderer);
//
//
//            XYSeriesRenderer uploadRenderer = new XYSeriesRenderer();
//            XYSeriesRenderer.FillOutsideLine uploadFill = new XYSeriesRenderer.FillOutsideLine(XYSeriesRenderer.FillOutsideLine.Type.BOUNDS_ALL);
//            uploadFill.setColor(Color.parseColor("#4d5a6a"));
//            uploadRenderer.addFillOutsideLine(uploadFill);
//            uploadRenderer.setDisplayChartValues(false);
//            uploadRenderer.setColor(Color.parseColor("#4d5a6a"));
//            uploadRenderer.setShowLegendItem(false);
//            uploadRenderer.setLineWidth(5);
//            final XYMultipleSeriesRenderer multiUploadRenderer = new XYMultipleSeriesRenderer();
//            multiUploadRenderer.setXLabels(0);
//            multiUploadRenderer.setYLabels(0);
//            multiUploadRenderer.setZoomEnabled(false);
//            multiUploadRenderer.setXAxisColor(Color.parseColor("#647488"));
//            multiUploadRenderer.setYAxisColor(Color.parseColor("#2F3C4C"));
//            multiUploadRenderer.setPanEnabled(false, false);
//            multiUploadRenderer.setZoomButtonsVisible(false);
//            multiUploadRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));
//            multiUploadRenderer.addSeriesRenderer(uploadRenderer);
//            final LinearLayout chartPing = mFragmentView.findViewById(R.id.chartPing);
//            final LinearLayout chartDownload = mFragmentView.findViewById(R.id.chartDownload);
//            final LinearLayout chartUpload = mFragmentView.findViewById(R.id.chartUpload);
//            //Reset value, graphics
//            mActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    mPingTextView.setText(mThreadSpeedTest.getmPingTextViewString());
//                    mDownloadTextView.setText(mThreadSpeedTest.getmDownloadTextViewString());
//                    mDownloadTextView2.setText(mThreadSpeedTest.getmDownloadTextViewString());
//                    mUploadTextView.setText(mThreadSpeedTest.getmUploadTextViewString());
//                    mUploadTextView2.setText(mThreadSpeedTest.getmUploadTextViewString());
//                }
//            });
//            mActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    while (!mThreadSpeedTest.getAllFinished()) {
//
//                        rotate = new RotateAnimation(mThreadSpeedTest.getLastPosition(), mThreadSpeedTest.getPosition(), Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                        rotate.setInterpolator(new FastOutLinearInInterpolator());
//                        rotate.setDuration(100);
//                        mClockWise.startAnimation(rotate);
//                    }
//                }
//
//            });
//            //Thread bitiminde button yeniden aktif ediliyor
//            mActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    while (true) {
//
//                        if (mThreadSpeedTest.getAllFinished()) {
//                            addHistoryItem();
//                            return;
//                        }
//                    }
//                }
//            });
//        }
//    };
//
//    public static Thread performOnBackgroundThread(final Runnable runnable) {
//        final Thread t = new Thread() {
//            @Override
//            public void run() {
//                try {
//                    runnable.run();
//                } finally {
//
//                }
//            }
//        };
//        return t;
//    }
//}
