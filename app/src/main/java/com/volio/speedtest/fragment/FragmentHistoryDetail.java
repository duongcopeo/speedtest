package com.volio.speedtest.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.volio.speedtest.HistoryItem;
import com.volio.speedtest.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class FragmentHistoryDetail extends Fragment {
    private HistoryItem historyItem;

    public void setHistoryItem(HistoryItem historyItem) {
        this.historyItem = historyItem;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView=inflater.inflate(R.layout.fragment_history_detail,container,false);
        Log.i("FragmentHistoryDetail","onCreateView");
        ImageView icNetworkImageView=mView.findViewById(R.id.iconNetworkImageView);
        TextView nameNetworkTextView=mView.findViewById(R.id.nameNetworkTextView);
        TextView uploadTextView=mView.findViewById(R.id.uploadTextView);
        TextView downloadTextView=mView.findViewById(R.id.downloadTextView);
        TextView pingTextView=mView.findViewById(R.id.pingTextView);
        TextView dowloadDataTextView=mView.findViewById(R.id.dowloadDataTextView);
        TextView uploadDataTextView=mView.findViewById(R.id.uploadDataTextView);
        TextView latitudeTextView=mView.findViewById(R.id.latitudeTextView);
        TextView longitudeTextView=mView.findViewById(R.id.longtitideTextView);
        TextView externalIPTextView=mView.findViewById(R.id.externalIPTextView);
        TextView internalIPTextView=mView.findViewById(R.id.internalIPTextView);
        TextView dateTimeTextView=mView.findViewById(R.id.timeNetworkTextView);
        if(historyItem.getmTypeNetwork()==1){
            icNetworkImageView.setImageDrawable(getResources().getDrawable(R.drawable.item_wifi));
        }
        else{
            icNetworkImageView.setImageDrawable(getResources().getDrawable(R.drawable.item_3g));
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date;
        try {
            date=df.parse(historyItem.getmDateTime());
            df=new SimpleDateFormat("HH:mm");
            String dateTimeString=df.format(date);
            nameNetworkTextView.setText(historyItem.getmNameNetwork());
            dateTimeTextView.setText(dateTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String partsRateString[]=historyItem.getmUploadRate().split("\\s+");
        uploadTextView.setText(partsRateString[0]);
        partsRateString=historyItem.getmDownloadRate().split("\\s+");
        downloadTextView.setText(partsRateString[0]);
        partsRateString=historyItem.getmPingRate().split("\\s+");
        pingTextView.setText(partsRateString[0]);
        dowloadDataTextView.setText(historyItem.getmDowloadData());
        uploadDataTextView.setText(historyItem.getmUploadData());
        latitudeTextView.setText(historyItem.getmLatitude());
        longitudeTextView.setText(historyItem.getmLongitude());
        externalIPTextView.setText(historyItem.getmExternalIp());
        internalIPTextView.setText(historyItem.getmInternalIp());

        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    public static FragmentHistoryDetail newInstance(){
        FragmentHistoryDetail fragmentHistoryDetail=new FragmentHistoryDetail();
        return fragmentHistoryDetail;
    }
}
