package com.volio.speedtest.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.volio.speedtest.HistoryItem;
import com.volio.speedtest.R;
import com.volio.speedtest.adapter.FragmentAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class FragmentSpeedDetail extends Fragment {
    private HistoryItem historyItem;

    public void setHistoryItem(HistoryItem historyItem) {
        this.historyItem = historyItem;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i("FragmentSpeedDetail","onCreateView");
        View mView=inflater.inflate(R.layout.fragment_speed_detail,container,false);
        ImageView icNetworkImageView=mView.findViewById(R.id.iconNetworkImageView);
        TextView nameNetworkTextView=mView.findViewById(R.id.nameNetworkTextView);
        TextView uploadTextView=mView.findViewById(R.id.uploadTextView);
        TextView downloadTextView=mView.findViewById(R.id.downloadTextView);
        TextView pingTextView=mView.findViewById(R.id.pingTextView);
        Button btnSpeedTestAgain=mView.findViewById(R.id.testAgainButton);
        TextView  dateTimeTextView=mView.findViewById(R.id.timeNetworkTextView);
        btnSpeedTestAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentHistoryListenerAgain.onSwitchToNextFragment2();
            }
        });
        if(historyItem.getmTypeNetwork()==1){
            icNetworkImageView.setImageDrawable(getResources().getDrawable(R.drawable.item_wifi));
        }
        else{
            icNetworkImageView.setImageDrawable(getResources().getDrawable(R.drawable.item_3g));
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date;
        try {
            date=df.parse(historyItem.getmDateTime());
            df=new SimpleDateFormat("HH:mm");
            String dateTimeString=df.format(date);
            nameNetworkTextView.setText(historyItem.getmNameNetwork());
            dateTimeTextView.setText(dateTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String partsRateString[]=historyItem.getmUploadRate().split("\\s+");
        uploadTextView.setText(partsRateString[0]);
        partsRateString=historyItem.getmDownloadRate().split("\\s+");
        downloadTextView.setText(partsRateString[0]);
        partsRateString=historyItem.getmPingRate().split("\\s+");
        pingTextView.setText(partsRateString[0]);
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    public static Fragment newInstance(){
        FragmentSpeedDetail fragmentSpeedDetail=new FragmentSpeedDetail();
        return fragmentSpeedDetail;
    }
    private FragmentAdapter.FragmentHistoryListenerAgain fragmentHistoryListenerAgain;
    public  void setFragmentHistoryListenerAgain(FragmentAdapter.FragmentHistoryListenerAgain fragmentHistoryListenerAgain){
        this.fragmentHistoryListenerAgain=fragmentHistoryListenerAgain;
    }
}
