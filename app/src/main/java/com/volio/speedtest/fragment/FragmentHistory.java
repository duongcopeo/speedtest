package com.volio.speedtest.fragment;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.volio.speedtest.HistoryItem;
import com.volio.speedtest.R;
import com.volio.speedtest.adapter.FragmentAdapter;
import com.volio.speedtest.adapter.RecyclerHistoryAdapter;
import com.volio.speedtest.callback.ClickHistoryItemCallBack;
import com.volio.speedtest.callback.SpeedTestCallBack;
import com.volio.speedtest.io.IOFile;
import com.volio.speedtest.service.BackgroundTestService2;
import com.volio.speedtest.viewmodel.SharedViewModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public  class FragmentHistory extends Fragment {
    private Context mContext;
    private Activity mActivity;
    private RecyclerView mHistoryRecyclerView;
    private RecyclerHistoryAdapter mRecyclerViewAdapter;
    private List<HistoryItem> mListHistoryItems = new ArrayList<>();
    private static final String FILECACHE = "speed_test_data.dat";
    View v1;
    private RelativeLayout whiteCircleRelativeLayout1;
    private RelativeLayout whiteCircleRelativeLayout2;
    private RelativeLayout whiteCircleRelativeLayout3;
    private RelativeLayout whiteCircleRelativeLayout4;
    private RelativeLayout whiteRectangleRelativeLayout1;
    private RelativeLayout whiteRectangleRelativeLayout2;
    private RelativeLayout whiteRectangleRelativeLayout3;
    private TextView txt_white1;
    private TextView txt_white2;
    private TextView txt_white3;
    private TextView txt_white4;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v1 = inflater.inflate(R.layout.fragment_history, container, false);
        IOFile.readFile(mListHistoryItems, FILECACHE, mContext);
        createListHistoryItem();
        initView();
        SharedViewModel sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        sharedViewModel.getHistoryItem().observe(this, new Observer<HistoryItem>() {
                    @Override
                    public void onChanged(HistoryItem historyItem) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mListHistoryItems.add(historyItem);
                                changeListHistoryItems(historyItem);
                                mRecyclerViewAdapter.notifyDataSetChanged();
                            }
                        });

                    }
                }


        );

        return v1;
    }

    private HistoryItem historyItem;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = (Activity) context;
    }

    Drawable shapeWhiteCirle;
    Drawable shapeBlackCirle;
    Drawable shapeWhiteRectangle;
    Drawable shapeBlackRectangle;
    DisplayMetrics metrics;

    public void initView() {
        whiteCircleRelativeLayout1 = v1.findViewById(R.id.white_circle1);
        whiteCircleRelativeLayout2 = v1.findViewById(R.id.white_circle2);
        whiteCircleRelativeLayout3 = v1.findViewById(R.id.white_circle3);
        whiteCircleRelativeLayout4 = v1.findViewById(R.id.white_circle4);
        txt_white1=v1.findViewById(R.id.txt_white1);
        txt_white2=v1.findViewById(R.id.txt_white2);
        txt_white3=v1.findViewById(R.id.txt_white3);
        txt_white4=v1.findViewById(R.id.txt_white4);
        whiteRectangleRelativeLayout1 = v1.findViewById(R.id.white_rectangle1);
        whiteRectangleRelativeLayout2 = v1.findViewById(R.id.white_rectangle2);
        whiteRectangleRelativeLayout3 = v1.findViewById(R.id.white_rectangle3);
        mHistoryRecyclerView = v1.findViewById(R.id.recycler_view_history);
        mHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        whiteRectangleRelativeLayout1.setOnClickListener(new ClickListener());
        whiteRectangleRelativeLayout2.setOnClickListener(new ClickListener());
        whiteRectangleRelativeLayout3.setOnClickListener(new ClickListener());
        whiteCircleRelativeLayout1.setOnClickListener(new ClickListener());
        whiteCircleRelativeLayout2.setOnClickListener(new ClickListener());
        whiteCircleRelativeLayout3.setOnClickListener(new ClickListener());
        whiteCircleRelativeLayout4.setOnClickListener(new ClickListener());
        shapeWhiteCirle = getResources().getDrawable(R.drawable.shape_white_circle);
        shapeBlackCirle = getResources().getDrawable(R.drawable.shape_black_circle);
        shapeWhiteRectangle = getResources().getDrawable(R.drawable.shape_rectangle_white);
        shapeBlackRectangle = getResources().getDrawable(R.drawable.shape_rectangle_black);
        metrics = getContext().getResources().getDisplayMetrics();
        mRecyclerViewAdapter = new RecyclerHistoryAdapter(mListHistoryItems);
        mRecyclerViewAdapter.setmClickHistoryItemCallBack(new ClickHistoryItemCallBack() {
            @Override
            public void onClickItem(int position) {
                HistoryItem historyItem=mRecyclerViewAdapter.getmListHistoryArray().get(position);
                fragmentHistoryListener.onSwitchToNextFragment(historyItem);
            }
        });
        mHistoryRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("FragmentHistory","onDestroyView");
        IOFile.writeFile(mListHistoryItems, FILECACHE, mContext);
    }

    int orderWhiteCircle = 1;
    ArrayList<HistoryItem> arrayList1Month;
    ArrayList<HistoryItem> arrayList1Week;
    ArrayList<HistoryItem> arrayListToday;

    private void changeListHistoryItems(HistoryItem item) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date;
        Date date1 = new Date();
        try {
            date = df.parse(item.getmDateTime());
            long diff = date1.getTime() - date.getTime();
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            if (days <= 1) {
                arrayListToday.add(item);
            } else if (days <= 30 && days > 7) {
                arrayList1Month.add(item);
            } else if (days > 1 && days <= 7) {
                arrayList1Week.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createListHistoryItem() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date;
        Date date1 = new Date();
        arrayList1Month = new ArrayList<>();
        arrayList1Week = new ArrayList<>();
        arrayListToday = new ArrayList<>();
        for (HistoryItem item : mListHistoryItems) {
            try {
                date = df.parse(item.getmDateTime());
                long diff = date1.getTime() - date.getTime();
                long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                Log.i("FragmentHistory",""+days);
                if (days <= 1) {
                    arrayListToday.add(item);
                } else if (days <= 30 && days > 7) {
                    arrayList1Month.add(item);
                } else if ( days >= 7&&days <30) {
                    arrayList1Week.add(item);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int idView = v.getId();
            switch (idView) {
                case R.id.white_circle4:
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            resetSizeCircleWhite(orderWhiteCircle);
                            whiteCircleRelativeLayout1.setBackground(shapeWhiteCirle);
                            whiteCircleRelativeLayout2.setBackground(shapeWhiteCirle);
                            whiteCircleRelativeLayout3.setBackground(shapeWhiteCirle);
                            whiteCircleRelativeLayout4.setBackground(shapeWhiteCirle);
                            animationHeight(whiteCircleRelativeLayout4);
                            animationWidth(whiteCircleRelativeLayout4);
                            txt_white4.setTextSize(12);
                            orderWhiteCircle = 4;
                            mRecyclerViewAdapter.setmListHistoryArray(mListHistoryItems);
                            mRecyclerViewAdapter.notifyDataSetChanged();
                        }
                    });
                    break;
                case R.id.white_circle3:
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            resetSizeCircleWhite(orderWhiteCircle);
                            whiteCircleRelativeLayout1.setBackground(shapeWhiteCirle);
                            whiteCircleRelativeLayout2.setBackground(shapeWhiteCirle);
                            whiteCircleRelativeLayout3.setBackground(shapeWhiteCirle);
                            whiteCircleRelativeLayout4.setBackground(shapeBlackCirle);
                            animationHeight(whiteCircleRelativeLayout3);
                            animationWidth(whiteCircleRelativeLayout3);
                            txt_white3.setTextSize(12);
                            whiteRectangleRelativeLayout1.setBackground(shapeWhiteRectangle);
                            whiteRectangleRelativeLayout2.setBackground(shapeWhiteRectangle);
                            whiteRectangleRelativeLayout3.setBackground(shapeBlackRectangle);
                            orderWhiteCircle = 3;
                            mRecyclerViewAdapter.setmListHistoryArray(arrayList1Month);
                            mRecyclerViewAdapter.notifyDataSetChanged();
                        }
                    });
                    break;
                case R.id.white_circle2:
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            resetSizeCircleWhite(orderWhiteCircle);
                            whiteCircleRelativeLayout1.setBackground(shapeWhiteCirle);
                            whiteCircleRelativeLayout2.setBackground(shapeWhiteCirle);
                            whiteCircleRelativeLayout3.setBackground(shapeBlackCirle);
                            whiteCircleRelativeLayout4.setBackground(shapeBlackCirle);
                            animationHeight(whiteCircleRelativeLayout2);
                            animationWidth(whiteCircleRelativeLayout2);
                            txt_white2.setTextSize(12);
                            whiteRectangleRelativeLayout1.setBackground(shapeWhiteRectangle);
                            whiteRectangleRelativeLayout2.setBackground(shapeBlackRectangle);
                            whiteRectangleRelativeLayout3.setBackground(shapeBlackRectangle);
                            orderWhiteCircle = 2;
                            mRecyclerViewAdapter.setmListHistoryArray(arrayList1Week);
                            mRecyclerViewAdapter.notifyDataSetChanged();
                        }
                    });
                    break;
                case R.id.white_circle1:
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            resetSizeCircleWhite(orderWhiteCircle);
                            whiteCircleRelativeLayout1.setBackground(shapeWhiteCirle);
                            whiteCircleRelativeLayout2.setBackground(shapeBlackCirle);
                            whiteCircleRelativeLayout3.setBackground(shapeBlackCirle);
                            whiteCircleRelativeLayout4.setBackground(shapeBlackCirle);
                            animationHeight(whiteCircleRelativeLayout1);
                            animationWidth(whiteCircleRelativeLayout1);
                            txt_white1.setTextSize(12);
                            whiteRectangleRelativeLayout1.setBackground(shapeBlackRectangle);
                            whiteRectangleRelativeLayout2.setBackground(shapeBlackRectangle);
                            whiteRectangleRelativeLayout3.setBackground(shapeBlackRectangle);
                            orderWhiteCircle = 1;
                            mRecyclerViewAdapter.setmListHistoryArray(arrayListToday);
                            mRecyclerViewAdapter.notifyDataSetChanged();
                        }
                    });
                    break;

            }
        }

        private void resetSizeCircleWhite(int order) {

            float dp = 20f;
            float fpixels = metrics.density * dp;
            int pixels = (int) (fpixels + 0.5f);
            View v = null;
            if (order == 1) {
                v = whiteCircleRelativeLayout1;
                txt_white1.setTextSize(8);
            } else if (order == 2) {
                txt_white2.setTextSize(8);
                v = whiteCircleRelativeLayout2;
            } else if (order == 3) {
                v = whiteCircleRelativeLayout3;
                txt_white3.setTextSize(8);
            } else if (order == 4) {
                txt_white4.setTextSize(8);
                v = whiteCircleRelativeLayout4;
            }
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) v.getLayoutParams();
            params.height = pixels;
            params.width = pixels;

            v.setLayoutParams(params);
        }

        private void animationHeight(View v) {
            float dp = 30f;
            float fpixels = metrics.density * dp;
            int pixels = (int) (fpixels + 0.5f);
            ValueAnimator anim = ValueAnimator.ofInt(v.getMeasuredHeight(), pixels);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
                    layoutParams.height = val;
                    v.setLayoutParams(layoutParams);
                }
            });
            anim.setDuration(50);
            anim.start();
        }

        private void animationWidth(View v) {
            float dp = 30f;
            float fpixels = metrics.density * dp;
            int pixels = (int) (fpixels + 0.5f);
            ValueAnimator anim = ValueAnimator.ofInt(v.getMeasuredWidth(), pixels);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
                    layoutParams.width = val;
                    v.setLayoutParams(layoutParams);
                }
            });
            anim.setDuration(50);
            anim.start();
        }
    }
    private FragmentAdapter.FragmentHistoryListener fragmentHistoryListener;
    public  void setFragmentHistoryListener(FragmentAdapter.FragmentHistoryListener fragmentHistoryListener){
        this.fragmentHistoryListener=fragmentHistoryListener;
    }
    public static FragmentHistory newInstance(){
        FragmentHistory fragmentHistory=new FragmentHistory();
        return fragmentHistory;
    }


}
