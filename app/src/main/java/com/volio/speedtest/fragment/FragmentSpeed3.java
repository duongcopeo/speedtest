//package com.volio.speedtest.fragment;
//
//import android.Manifest;
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.pm.PackageManager;
//import android.graphics.Color;
//import android.location.Location;
//import android.net.Uri;
//import android.net.wifi.WifiInfo;
//import android.net.wifi.WifiManager;
//import android.os.Bundle;
//import android.provider.Settings;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.view.animation.LinearInterpolator;
//import android.view.animation.RotateAnimation;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.location.FusedLocationProviderClient;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.android.material.snackbar.Snackbar;
//import com.volio.speedtest.BroatcastReceiver.BroatcastSpeedTest;
//import com.volio.speedtest.BuildConfig;
//import com.volio.speedtest.GetSpeedTestHostsHandler;
//import com.volio.speedtest.HistoryItem;
//import com.volio.speedtest.Main2Activity;
//import com.volio.speedtest.R;
//import com.volio.speedtest.callback.SpeedTestCallBack;
//import com.volio.speedtest.network.Connectivity;
//import com.volio.speedtest.service.BackgroundTestService;
//import com.volio.speedtest.service.BackgroundTestService2;
//import com.volio.speedtest.test.HttpDownloadTest;
//import com.volio.speedtest.test.HttpUploadTest;
//import com.volio.speedtest.test.PingTest;
//import com.volio.speedtest.viewmodel.SharedViewModel;
//
//import org.achartengine.ChartFactory;
//import org.achartengine.GraphicalView;
//import org.achartengine.model.XYMultipleSeriesDataset;
//import org.achartengine.model.XYSeries;
//import org.achartengine.renderer.XYMultipleSeriesRenderer;
//import org.achartengine.renderer.XYSeriesRenderer;
//
//import java.net.DatagramSocket;
//import java.net.InetAddress;
//import java.net.SocketException;
//import java.net.UnknownHostException;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.LinkedHashMap;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.core.app.ActivityCompat;
//import androidx.fragment.app.Fragment;
//import androidx.interpolator.view.animation.FastOutLinearInInterpolator;
//import androidx.lifecycle.ViewModelProviders;
//
//public class FragmentSpeed3 extends Fragment {
//    private View mFragmentView;
//    private Activity mActivity;
//    private final static String TAG = Main2Activity.class.getSimpleName();
//    private View mViewMainActivity;
//    private Button mStartButton;
//    private ImageView mIconNetworkImageView;
//    private TextView mNameNetworkTextView;
//    private LinearLayout mUploadLinearLayout;
//    private LinearLayout mDownloadLinearLayout;
//    private Context mContext;
//    private static float position = 0;
//    private static float lastPosition = 0;
//    private GetSpeedTestHostsHandler getSpeedTestHostsHandler = null;
//    private HashSet<String> tempBlackList;
//    private double selfLat = 0.0;
//    private double selfLon = 0.0;
//    private String mUploadString;
//    private String mDownloadString;
//    private String mPingString;
//    private String mDateTimeString;
//    private String mExternalIp;
//    private String mInternalIp;
//
//    private SpeedTestCallBack speedTestCallBack;
//    private BackgroundTestService mService = null;
//    final DecimalFormat dec = new DecimalFormat("#.##");
//    private boolean mBound = false;
//    private BroatcastSpeedTest broatcastSpeedTest;
//    private IntentFilter filter;
//
//    public void setSpeedTestCallBack(Main2Activity activity) {
//        speedTestCallBack = activity;
//    }
//
//    Intent intentStartService;
//    RotateAnimation rotate;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
//        mFragmentView = inflater.inflate(R.layout.fragment_speed, container, false);
//        mViewMainActivity = container;
//        intentStartService = new Intent(mActivity, BackgroundTestService2.class);
//        mFusedLocationProviderClient = new FusedLocationProviderClient(mActivity);
//        mUploadLinearLayout = mFragmentView.findViewById(R.id.UploadLinearLayout);
//        mDownloadLinearLayout = mFragmentView.findViewById(R.id.DownloadLinearLayout);
//        RelativeLayout mClockWise = mFragmentView.findViewById(R.id.barImageView2);
//        TextView mPingTextView = mFragmentView.findViewById(R.id.pingTextView);
//        TextView mUploadTextView = mFragmentView.findViewById(R.id.uploadTextView);
//        TextView mDownloadTextView = mFragmentView.findViewById(R.id.downloadTextView);
//        TextView mDownloadTextView2 = mFragmentView.findViewById(R.id.downloadTextView2);
//        TextView mUploadTextView2 = mFragmentView.findViewById(R.id.uploadTextView2);
//        mIconNetworkImageView = mFragmentView.findViewById(R.id.ic_network);
//        mNameNetworkTextView = mFragmentView.findViewById(R.id.name_network);
//        mStartButton = mFragmentView.findViewById(R.id.startButton2);
//        mStartButton.setText("SPEED TEST");
//        restartSpeedTestHostsHandler();
//        OnBtnClickListener onBtnClickListener = new OnBtnClickListener();
//        mStartButton.setOnClickListener(onBtnClickListener);
//        filter = new IntentFilter();
//        filter.addAction("volio");
//        filter.setPriority(3);
//        broatcastSpeedTest = new BroatcastSpeedTest() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (intent != null) {
//                    int number;
//                    number = intent.getIntExtra("typeCountinue", -1);
//                    if (number == 1) {
//                        Toast.makeText(mContext, "No Connnection...", Toast.LENGTH_SHORT).show();
//                        mStartButton.setEnabled(true);
//                        mStartButton.setTextSize(16);
//                        mStartButton.setText("RESTART TEST");
//                    } else if (number == 2) {
//                        String info2 = intent.getStringExtra("info");
//                        double distance2 = intent.getDoubleExtra("distance", -1);
//                        selfLat = intent.getDoubleExtra("selfLat", -1);
//                        selfLon = intent.getDoubleExtra("selftLon", -1);
//                        mStartButton.setTextSize(13);
//                        mStartButton.setText(String.format("Host Location: %s [Distance: %s km]", info2, dec.format(distance2 / 1000)));
//                    } else if (number == 0) {
//                        mStartButton.setEnabled(false);
//                        mStartButton.setText("Selecting best server based on Distance...");
//                    } else if (number == 3) {
//                        mPingTextView.setText("0 ms");
//                        mDownloadTextView.setText("0 Mbps");
//                        mUploadTextView.setText("0 Mbps");
//                    } else if (number == 4) {
//                        mPingString = intent.getDoubleExtra("pingTestAvg", -1) + " ms";
//                        mPingTextView.setText(mPingString);
//                    } else if (number == 5) {
//                        mPingTextView.setText(dec.format(intent.getDoubleExtra("pingTestInstant", -1)) + " ms");
//                    } else if (number == 6) {
//                        mDownloadString = dec.format(intent.getDoubleExtra("downloadTestFinal", -1)) + " Mbps";
//                        mDownloadTextView.setText(mDownloadString);
//                    } else if (number == 7) {
//                        position = intent.getFloatExtra("position", -1);
//                        lastPosition = intent.getFloatExtra("lastposition", -1);
//                        mActivity.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                rotate = new RotateAnimation(lastPosition, position, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                                rotate.setInterpolator(new FastOutLinearInInterpolator());
//                                rotate.setDuration(100);
//                                mClockWise.startAnimation(rotate);
//                            }
//                        });
//                        mDownloadTextView.setText(dec.format(intent.getDoubleExtra("downloadTestInstant", -1)) + " Mbps");
//                    } else if (number == 8) {
//                        mUploadString = dec.format(intent.getDoubleExtra("uploadTestFinal", -1)) + " Mbps";
//                        mUploadTextView.setText(mUploadString);
//                    } else if (number == 9) {
//                        position = intent.getFloatExtra("position", -1);
//                        lastPosition = intent.getFloatExtra("lastposition", -1);
//                        rotate = new RotateAnimation(lastPosition, position, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                        mActivity.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                rotate.setInterpolator(new FastOutLinearInInterpolator());
//                                rotate.setDuration(100);
//                                mClockWise.startAnimation(rotate);
//                            }
//                        });
//                        mUploadTextView.setText(dec.format(intent.getDoubleExtra("uploadTestInstant", -1)) + " Mbps");
//                    } else if (number == 10) {
//                        mStartButton.setEnabled(true);
//                        mStartButton.setTextSize(16);
//                        mStartButton.setText("Restart Test");
//                        mDateTimeString = intent.getStringExtra("dateTimeString");
//                        mExternalIp = intent.getStringExtra("externalIP");
//                        mInternalIp = intent.getStringExtra("internalIP");
//                        addHistoryItem(mUploadString, mDownloadString, mPingString, nameNetWork, typeNetWork, mDateTimeString,
//                                selfLat, selfLon, mExternalIp, mInternalIp);
//                    }
//                }
//            }
//        };
//        mActivity.registerReceiver(broatcastSpeedTest, filter);
//
//        return mFragmentView;
//    }
//
//    private SharedViewModel sharedViewModel;
//
//    private void addHistoryItem(String mUploadString, String mDownloadString,
//                                String mPingString, String nameNetWork, int typeNetWork,
//                                String mDateTimeString, double selfLat, double selfLon, String mExternalIp, String mInternalIp
//    ) {
//        HistoryItem historyItem = new HistoryItem(mUploadString, mDownloadString, mPingString,
//                nameNetWork, typeNetWork, mDateTimeString, "" + selfLat, "" + selfLon, mExternalIp, mInternalIp,"","");
//        sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
//        sharedViewModel.setHistoryItem(historyItem);
//    }
//
//
//    private boolean checkPermissions(String accessCoarseLocation) {
//        int permissionState = ActivityCompat.checkSelfPermission(mContext, accessCoarseLocation);
//        return permissionState == PackageManager.PERMISSION_GRANTED;
//    }
//
//    //========================================BEHAVIOUR METHOD TO GET SPEED TEST==============//
//    public void restartSpeedTestHostsHandler() {
//        getSpeedTestHostsHandler = new GetSpeedTestHostsHandler();
//        getSpeedTestHostsHandler.start();
//    }
//
//
//    //FROM_HERE-----------------------MPS_TO_ANPHA-----------------------------------------
//    public float getPositionByRate(double rate) {
//        if (rate >= 0 && rate <= 120) {
//            return (float) rate * 2;
//        }
//        return 0;
//    }
//
//    //FROM_HERE-----------------------DEFAULT-----------------------------------------
//    public void showSnackBar(final String txt) {
//        View v = mViewMainActivity.findViewById(R.id.main_activity_container2);
//        if (v != null) {
//            Snackbar.make(v, txt, Snackbar.LENGTH_LONG).show();
//        }
//    }
//
//    public void showSnackBar(final int mainTextString, final int textActionString, View.OnClickListener listener) {
//        Snackbar.make(mViewMainActivity.findViewById(R.id.main_activity_container2),
//                getString(mainTextString),
//                Snackbar.LENGTH_INDEFINITE).setAction(getString(textActionString), listener).show();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (!checkPermissions(Manifest.permission.ACCESS_COARSE_LOCATION)) {
//            requestPermission();
//        } else {
//            getLastLotation();
//        }
//        restartSpeedTestHostsHandler();
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        mActivity.unregisterReceiver(broatcastSpeedTest);
//    }
//
//    private void requestPermission() {
//        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION);
//        if (shouldProvideRationale) {
//            Log.i(TAG, "Displaying permission to provide additional context");
//            showSnackBar(R.string.permission_rationale, android.R.string.ok, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    startRequestPermissionResult();
//                }
//            });
//        } else {
//            Log.i(TAG, "Requesting permission");
//            startRequestPermissionResult();
//        }
//    }
//
//    private int REQUEST_PERMISSION_CODE_ALL = 32;
//
//    private void startRequestPermissionResult() {
//        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSION_CODE_ALL);
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        Log.i(TAG, "onRequestPermissionsResult");
//        if (grantResults.length <= 0) {
//            Log.i(TAG, "User interaction was cancelled.");
//        } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            getLastLotation();
//        } else {
//            showSnackBar(R.string.permission_denied_need, R.string.settings, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent();
//                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                    Uri uri = Uri.fromParts("pakage", BuildConfig.APPLICATION_ID, null);
//                    intent.setData(uri);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
//            });
//        }
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//
//    private FusedLocationProviderClient mFusedLocationProviderClient;
//
//    @SuppressLint("MissingPermission")
//    private void getLastLotation() {
//        mFusedLocationProviderClient.getLastLocation()
//                .addOnCompleteListener(new OnCompleteListener<Location>() {
//                    @Override
//                    public void onComplete(Task<Location> task) {
//                        if (task.getResult() != null && task.isSuccessful()) {
//                            selfLat = task.getResult().getLatitude();
//                            selfLon = task.getResult().getLongitude();
//                        } else {
//                            Log.e(TAG, "get last lotation exception");
//                            showSnackBar(getString(R.string.no_lotation_detected));
//                        }
//                    }
//                });
//
//    }
//
//    private OnFragmentInteractionListener mListener;
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        mContext = context;
//        mActivity = (Activity) context;
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//        }
//    }
//
//    public static HashMap<Integer, Float> sortByValue(HashMap<Integer, Float> hm) {
//        // Create a list from elements of HashMap
//        List<Map.Entry<Integer, Float>> list =
//                new LinkedList<Map.Entry<Integer, Float>>(hm.entrySet());
//
//        // Sort the list
//        Collections.sort(list, new Comparator<Map.Entry<Integer, Float>>() {
//            public int compare(Map.Entry<Integer, Float> o1,
//                               Map.Entry<Integer, Float> o2) {
//                return (o1.getValue()).compareTo(o2.getValue());
//            }
//        });
//
//        // put data from sorted list to hashmap
//        HashMap<Integer, Float> temp = new LinkedHashMap<Integer, Float>();
//        for (Map.Entry<Integer, Float> aa : list) {
//            temp.put(aa.getKey(), aa.getValue());
//        }
//        return temp;
//    }
//
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
//
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    private int typeNetWork;
//    private String nameNetWork;
//
//    private void detectInformationNetwork() {
//
//        if (Connectivity.isConnected(mContext)) {
//            if (Connectivity.isConnectedWifi(mContext)) {
//                WifiManager wifiMgr = (WifiManager) mContext.getSystemService(mContext.WIFI_SERVICE);
//                WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
//                nameNetWork = wifiInfo.getSSID();
//                typeNetWork = 1;
//            } else if (Connectivity.isConnectedMobile(mContext)) {
//                nameNetWork = "VIETTEL";
//                typeNetWork = 2;
//
//            }
//        }
//    }
//
//    private void changeViewNetWork() {
//        if (typeNetWork == 1) {
//            mIconNetworkImageView.setImageDrawable(getResources().getDrawable(R.drawable.item_wifi));
//            mNameNetworkTextView.setText(nameNetWork);
//        } else if (typeNetWork == 2) {
//            mIconNetworkImageView.setImageDrawable(getResources().getDrawable(R.drawable.item_3g));
//            mNameNetworkTextView.setText(nameNetWork);
//        }
//    }
//
//    private class OnBtnClickListener implements View.OnClickListener {
//
//        @Override
//        public void onClick(View v) {
//            mActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    detectInformationNetwork();
//                    changeViewNetWork();
//                }
//            });
//            mActivity.startService(intentStartService);
//        }
//    }
//}
