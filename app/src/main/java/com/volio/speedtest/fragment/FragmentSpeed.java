package com.volio.speedtest.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.volio.speedtest.BuildConfig;
import com.volio.speedtest.GetSpeedTestHostsHandler;
import com.volio.speedtest.HistoryItem;
import com.volio.speedtest.Main2Activity;
import com.volio.speedtest.R;
import com.volio.speedtest.ThreadSpeedTest;
import com.volio.speedtest.callback.SpeedTestCallBack;
import com.volio.speedtest.network.Connectivity;
import com.volio.speedtest.service.BackgroundTestService;
import com.volio.speedtest.test.HttpDownloadTest;
import com.volio.speedtest.test.HttpUploadTest;
import com.volio.speedtest.test.PingTest;
import com.volio.speedtest.viewmodel.SharedViewModel;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.interpolator.view.animation.FastOutLinearInInterpolator;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class FragmentSpeed extends Fragment {
    private View mFragmentView;
    private Activity mActivity;
    private final static String TAG = Main2Activity.class.getSimpleName();
    private View mViewMainActivity;
    private Button mStartButton;
    private ImageView mIconNetworkImageView;
    private TextView mNameNetworkTextView;
    private LinearLayout mUploadLinearLayout;
    private LinearLayout mDownloadLinearLayout;
    private Context mContext;
    private static float position = 0;
    private static float lastPosition = 0;
    private GetSpeedTestHostsHandler getSpeedTestHostsHandler = null;
    private HashSet<String> tempBlackList;
    private double selfLat = 0.0;
    private double selfLon = 0.0;
    private String mUploadString;
    private String mDownloadString;
    private String mPingString;
    private String mDateTimeString;
    private String mExternalIp;
    private String mInternalIp;
    private int mUploadData;
    private int mDownloadData;
    private SpeedTestCallBack speedTestCallBack;
    private BackgroundTestService mService = null;
    final DecimalFormat dec = new DecimalFormat("#.##");
    private boolean mBound = false;
    private HttpDownloadTest downloadTest;
    private HttpUploadTest uploadTest;
    private PingTest pingTest;

    public void setSpeedTestCallBack(Main2Activity activity) {
        speedTestCallBack = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_speed, container, false);
        Log.i("FragmentSpeed", "onCreateView");
        mViewMainActivity = container;
        mFusedLocationProviderClient = new FusedLocationProviderClient(mActivity);
        mUploadLinearLayout = mFragmentView.findViewById(R.id.UploadLinearLayout);
        mDownloadLinearLayout = mFragmentView.findViewById(R.id.DownloadLinearLayout);
        mIconNetworkImageView = mFragmentView.findViewById(R.id.ic_network);
        mNameNetworkTextView = mFragmentView.findViewById(R.id.name_network);
        mStartButton = mFragmentView.findViewById(R.id.startButton2);
        mStartButton.setText("SPEED TEST");
        restartSpeedTestHostsHandler();
        OnBtnClickListener onBtnClickListener = new OnBtnClickListener();

        mStartButton.setOnClickListener(onBtnClickListener);
        return mFragmentView;
    }

    private SharedViewModel sharedViewModel;
    private void addHistoryItem() {
        HistoryItem historyItem = new HistoryItem(mUploadString, mDownloadString, mPingString,
                nameNetWork, typeNetWork, mDateTimeString, "" + selfLat, "" + selfLon, mExternalIp, mInternalIp,mUploadData+" MB",mDownloadData+" MB");

        if(getActivity()!=null)
        {
            sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
            sharedViewModel.setHistoryItem(historyItem);
        }
    }


    private boolean checkPermissions(String accessCoarseLocation) {
        int permissionState = ActivityCompat.checkSelfPermission(mContext, accessCoarseLocation);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    //========================================BEHAVIOUR METHOD TO GET SPEED TEST==============//
    public void restartSpeedTestHostsHandler() {
        getSpeedTestHostsHandler = new GetSpeedTestHostsHandler();
        getSpeedTestHostsHandler.start();
    }


    //FROM_HERE-----------------------MPS_TO_ANPHA-----------------------------------------
    public float getPositionByRate(double rate) {
        if (rate >= 0 && rate <= 120) {
            return (float) rate * 2;
        }
        return 0;
    }

    //FROM_HERE-----------------------DEFAULT-----------------------------------------
    public void showSnackBar(final String txt) {
        View v = mFragmentView;
        if (v != null) {
            Snackbar.make(v, txt, Snackbar.LENGTH_LONG).show();
        }
    }

    public void showSnackBar(final int mainTextString, final int textActionString, View.OnClickListener listener) {
        Snackbar.make(mViewMainActivity.findViewById(R.id.main_activity_container2),
                getString(mainTextString),
                Snackbar.LENGTH_INDEFINITE).setAction(getString(textActionString), listener).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("FragmentSpeed","onDestroy");
        if (!checkPermissions(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            requestPermission();
        } else {
            getLastLotation();
        }
        restartSpeedTestHostsHandler();
        Log.i(TAG, "onRequestPermissionsResult");
    }

    private void requestPermission() {
        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission to provide additional context");
            showSnackBar(R.string.permission_rationale, android.R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startRequestPermissionResult();
                }
            });
        } else {
            Log.i(TAG, "Requesting permission");
            startRequestPermissionResult();
        }
    }

    private int REQUEST_PERMISSION_CODE_ALL = 32;

    private void startRequestPermissionResult() {
        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSION_CODE_ALL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionsResult");
        if (grantResults.length <= 0) {
            Log.i(TAG, "User interaction was cancelled.");
        } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getLastLotation();
        } else {
            showSnackBar(R.string.permission_denied_need, R.string.settings, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("pakage", BuildConfig.APPLICATION_ID, null);
                    intent.setData(uri);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private FusedLocationProviderClient mFusedLocationProviderClient;

    @SuppressLint("MissingPermission")
    private void getLastLotation() {
        mFusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(Task<Location> task) {
                        if (task.getResult() != null && task.isSuccessful()) {
                            selfLat = task.getResult().getLatitude();
                            selfLon = task.getResult().getLongitude();
                        } else {
                            Log.e(TAG, "get last lotation exception");
                            showSnackBar(getString(R.string.no_lotation_detected));
                        }
                    }
                });

    }

    private OnFragmentInteractionListener mListener;

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("FragmentSpeed","onDestroy");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i("FragmentSpeed","onAttach");
        mContext = context;
        mActivity = (Activity) context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i("FragmentSpeed","onDetach");
        if(pingTest!=null){
            pingTest.interrupt();
            uploadTest.interrupt();
            downloadTest.interrupt();
        }
        if(coreThreadSpeedTest!=null)
        coreThreadSpeedTest.interrupt();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public static HashMap<Integer, Float> sortByValue(HashMap<Integer, Float> hm) {
        // Create a list from elements of HashMap
        List<Map.Entry<Integer, Float>> list =
                new LinkedList<Map.Entry<Integer, Float>>(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<Integer, Float>>() {
            public int compare(Map.Entry<Integer, Float> o1,
                               Map.Entry<Integer, Float> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<Integer, Float> temp = new LinkedHashMap<Integer, Float>();
        for (Map.Entry<Integer, Float> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private int typeNetWork;
    private String nameNetWork;

    private void detectInformationNetwork() {

        if (Connectivity.isConnected(mContext)) {
            if (Connectivity.isConnectedWifi(mContext)) {
                WifiManager wifiMgr = (WifiManager) mContext.getSystemService(mContext.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
                nameNetWork = wifiInfo.getSSID();
                typeNetWork = 1;
            } else if (Connectivity.isConnectedMobile(mContext)) {
                typeNetWork = 2;

            }
        }
    }

    private void changeViewNetWork() {
        if (typeNetWork == 1) {
            mIconNetworkImageView.setImageDrawable(getResources().getDrawable(R.drawable.item_wifi));
            mNameNetworkTextView.setText(nameNetWork);
        } else if (typeNetWork == 2) {
            mIconNetworkImageView.setImageDrawable(getResources().getDrawable(R.drawable.item_3g));
            mNameNetworkTextView.setText(nameNetWork);
        }
    }

    private class OnBtnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    detectInformationNetwork();
                    changeViewNetWork();
                }
            });

            mStartButton.setEnabled(false);
            if (getSpeedTestHostsHandler == null) {
                restartSpeedTestHostsHandler();
            }
            /*
            Caution: A service runs in the same process as the application in which
             it is declared and in the main thread of that application by default.
             If your service performs intensive or blocking operations while the user interacts
              with an activity from the same application,
              the service slows down activity performance. To avoid impacting application performance,
              start a new thread inside the service
              Service
              This is the base class for all services. When you extend this class,
              it's important to create a new thread in which the service can complete all of its work;
               the service uses your application's main thread by default,
                which can slow the performance of any activity that your application is running.
             */
            Runnable runnable2 = new Runnable() {
                RotateAnimation rotate;
                RelativeLayout mClockWise = mFragmentView.findViewById(R.id.barImageView2);
                TextView mPingTextView = mFragmentView.findViewById(R.id.pingTextView);
                TextView mDownloadTextView2 = mFragmentView.findViewById(R.id.downloadTextView2);
                TextView mUploadTextView2 = mFragmentView.findViewById(R.id.uploadTextView2);
                TextView mDisplaySpeedTextView = mFragmentView.findViewById(R.id.displaySpeedTextView);

                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //mStartButton.setText("Selecting best server based on Distance...");
                            showSnackBar("Selecting best server based on Distance...");
                        }
                    });
                    int timeCount = 600;
                    while (!getSpeedTestHostsHandler.isFinished()) {
                        timeCount--;
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (timeCount <= 0) {
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showSnackBar("No Connection...");
                                    mStartButton.setEnabled(true);
                                   // mStartButton.setTextSize(16);
                                    mStartButton.setText("RESTART TEST");
                                }
                            });
                            getSpeedTestHostsHandler = null;
                            return;
                        }
                    }
                    HashMap<Integer, String> mapKey = getSpeedTestHostsHandler.getMapKey();
                    HashMap<Integer, List<String>> mapValue = getSpeedTestHostsHandler.getMapValue();
                    if (selfLat == 0.0) {
                        selfLat = getSpeedTestHostsHandler.getSelfLat();
                        selfLon = getSpeedTestHostsHandler.getSelfLon();

                    }
                    double dist = 0.0;
                    int findServerIndex = 0;
                    double tmp = 19349458;
                    //calculate distance
                    for (int index : mapKey.keySet()) {

                        Location source = new Location("Source");
                        source.setLatitude(selfLat);
                        source.setLongitude(selfLon);

                        List<String> ls = mapValue.get(index);
                        Location dest = new Location("Dest");

                        dest.setLatitude(Double.parseDouble(ls.get(0)));
                        dest.setLongitude(Double.parseDouble(ls.get(1)));
                        double distance = source.distanceTo(dest);
                        if (tmp > distance) {
                            tmp = distance;
                            dist = distance;
                            findServerIndex = index;
                        }
                    }

                    final String uploadAddr = mapKey.get(findServerIndex);
                    final List<String> info = mapValue.get(findServerIndex);
                    final double distance = dist;
                    Log.e("quay len", info.get(6) + " " + selfLat + " " + selfLon);
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //mStartButton.setTextSize(13);
                           // mStartButton.setText(String.format("Host Location: %s [Distance: %s km]", info.get(2), dec.format(distance / 1000)));
                            showSnackBar(String.format("Host Location: %s [Distance: %s km]", info.get(2), dec.format(distance / 1000)));
                        }
                    });
                    //Reset value, graphics
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mUploadLinearLayout.setVisibility(View.VISIBLE);
                            mDownloadLinearLayout.setVisibility(View.VISIBLE);
                            mDownloadTextView2.setText("0 Mbps");
                            mUploadTextView2.setText("0 Mbps");
                        }
                    });


                    //Init Test
                    final List<Double> pingRateList = new ArrayList<>();
                    final List<Double> downloadRateList = new ArrayList<>();
                    final List<Double> uploadRateList = new ArrayList<>();
                    Boolean pingTestStarted = false;
                    Boolean pingTestFinished = false;
                    Boolean downloadTestStarted = false;
                    Boolean downloadTestFinished = false;
                    Boolean uploadTestStarted = false;
                    Boolean uploadTestFinished = false;
                     pingTest = new PingTest(info.get(6).replace(":8080", ""), 6);
                     downloadTest = new HttpDownloadTest(uploadAddr.replace(uploadAddr.split("/")[uploadAddr.split("/").length - 1], ""));
                     uploadTest = new HttpUploadTest(uploadAddr);
                    //Tests;
                    while (true) {
                        if (!pingTestStarted) {
                            pingTest.start();
                            pingTestStarted = true;
                        }
                        if (pingTestFinished && !downloadTestStarted) {
                            downloadTest.start();
                            downloadTestStarted = true;
                        }
                        if (downloadTestFinished && !uploadTestStarted) {
                            uploadTest.start();
                            uploadTestStarted = true;
                        }

                        //Ping Test
                        if (pingTestFinished) {
                            //Failure
                            if (pingTest.getAvgRtt() == 0) {
                                System.out.println("Ping error...");
                            } else {
                                //Success
                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDisplaySpeedTextView.setText(dec.format(pingTest.getAvgRtt()) + " ms");
                                    }
                                });
                            }
                        } else {
                            pingRateList.add(pingTest.getInstantRtt());

                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mDisplaySpeedTextView.setText(dec.format(pingTest.getInstantRtt()) + " ms");
                                }
                            });
                        }


                        //Download Test
                        if (pingTestFinished) {
                            if (downloadTestFinished) {
                                //Failure
                                if (downloadTest.getFinalDownloadRate() == 0) {
                                    System.out.println("Download error...");
                                } else {
                                    //Success
                                    mActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mDownloadTextView2.setText(dec.format(downloadTest.getFinalDownloadRate()) + " Mbps");
                                            mDisplaySpeedTextView.setText(dec.format(downloadTest.getFinalDownloadRate()) + " Mbps");

                                        }
                                    });
                                }
                            } else {
                                //Calc position
                                double downloadRate = downloadTest.getInstantDownloadRate();
                                downloadRateList.add(downloadRate);
                                position = getPositionByRate(downloadRate);

                                mActivity.runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        rotate = new RotateAnimation(lastPosition, position, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                        rotate.setInterpolator(new FastOutLinearInInterpolator());
                                        rotate.setDuration(100);
                                        mClockWise.startAnimation(rotate);
                                        mDownloadTextView2.setText(dec.format(downloadTest.getInstantDownloadRate()) + " Mbps");
                                        mDisplaySpeedTextView.setText(dec.format(downloadTest.getInstantDownloadRate()) + " Mbps");

                                    }

                                });
                                lastPosition = position;

                            }
                        }


                        //Upload Test
                        if (downloadTestFinished) {
                            if (uploadTestFinished) {
                                //Failure
                                if (uploadTest.getFinalUploadRate() == 0) {
                                    System.out.println("Upload error...");
                                } else {
                                    //Success
                                    mActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mUploadTextView2.setText(dec.format(uploadTest.getFinalUploadRate()) + " Mbps");
                                            mDisplaySpeedTextView.setText(dec.format(uploadTest.getFinalUploadRate()) + " Mbps");
                                        }
                                    });
                                }
                            } else {
                                //Calc position
                                double uploadRate = uploadTest.getInstantUploadRate();
                                uploadRateList.add(uploadRate);
                                position = getPositionByRate(uploadRate);

                                mActivity.runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        rotate = new RotateAnimation(lastPosition, position, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                        rotate.setInterpolator(new LinearInterpolator());
                                        rotate.setDuration(100);
                                        mClockWise.startAnimation(rotate);
                                        mUploadTextView2.setText(dec.format(uploadTest.getInstantUploadRate()) + " Mbps");
                                        mDisplaySpeedTextView.setText(dec.format(uploadTest.getInstantUploadRate()) + " Mbps");
                                    }

                                });
                                lastPosition = position;
                            }
                        }
                        //Test bitti
                        if (pingTestFinished && downloadTestFinished && uploadTest.isFinished()) {
                            break;
                        }

                        if (pingTest.isFinished()) {
                            pingTestFinished = true;
                        }
                        if (downloadTest.isFinished()) {
                            downloadTestFinished = true;
                        }
                        if (uploadTest.isFinished()) {
                            uploadTestFinished = true;
                        }

                        if (pingTestStarted && !pingTestFinished) {
                            try {
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                            }
                        } else {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                    //Thread bitiminde button yeniden aktif ediliyor
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mStartButton.setEnabled(true);
                            //mStartButton.setTextSize(16);
                            mStartButton.setText("Restart Test");
                        }
                    });
                    mUploadString = dec.format(uploadTest.getFinalUploadRate()) + " Mbps";
                    mDownloadString = dec.format(downloadTest.getFinalDownloadRate()) + " Mbps";
                    mPingString = dec.format(pingTest.getAvgRtt()) + " Ms";
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    Date date = new Date();
                    mDateTimeString = formatter.format(date);

                    try (
                            final DatagramSocket socket = new DatagramSocket())

                    {
                        socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
                        mExternalIp = socket.getLocalAddress().getHostAddress();
                    } catch (
                            SocketException e)

                    {
                        e.printStackTrace();
                    } catch (
                            UnknownHostException e)

                    {
                        e.printStackTrace();
                    }

                    mInternalIp = getSpeedTestHostsHandler.getmIP();
                    mDownloadData=downloadTest.getDowloadedByted()/1000000;
                    mUploadData=uploadTest.getUploadedKbyte()/(8*1000);
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            addHistoryItem();

                        }
                    });
                }

            };
            performOnBackgroundThread(runnable2);
        }
    }
    private Thread coreThreadSpeedTest;
    public  Thread performOnBackgroundThread(final Runnable runnable) {
        coreThreadSpeedTest=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } finally {

                }
            }
        });
        coreThreadSpeedTest.start();
        return coreThreadSpeedTest;
    }
    public static FragmentSpeed newInstance(){
        FragmentSpeed fragmentSpeed=new FragmentSpeed();
        return fragmentSpeed;
    }
}
