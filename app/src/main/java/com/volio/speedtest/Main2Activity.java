package com.volio.speedtest;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.volio.speedtest.adapter.FragmentAdapter;
import com.volio.speedtest.callback.SpeedTestCallBack;
import com.volio.speedtest.fragment.FragmentHistory;
import com.volio.speedtest.fragment.FragmentHistoryDetail;
import com.volio.speedtest.fragment.FragmentSpeed;
import com.volio.speedtest.fragment.FragmentSpeedDetail;
import com.volio.speedtest.service.BackgroundTestService;
import com.volio.speedtest.service.BackgroundTestService2;
import com.volio.speedtest.test.PingTest;
import com.volio.speedtest.viewmodel.SharedViewModel;
import com.volio.speedtest.service.BackgroundTestService.LocalBackgroundTestBinder;
import com.volio.speedtest.service.BackgroundTestService2.LocalBackgroundTestBinder2;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

public class Main2Activity  extends AppCompatActivity implements SpeedTestCallBack {
    private FragmentAdapter fragmentAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FragmentHistory fragmentHistory;
    private FragmentSpeed fragmentSpeed;
    private SharedViewModel sharedViewModel;
    private BackgroundTestService mService=null;
    private BackgroundTestService2 mService2;
    private boolean mBound=false;

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private ServiceConnection connection=new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocalBackgroundTestBinder2 binder=((LocalBackgroundTestBinder2)service);
            mService2=binder.getService();
            mBound=true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound=false;
        }
    };
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        viewPager = findViewById(R.id.view_paper);
        tabLayout = findViewById(R.id.tab_layout);
        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(fragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);
        sharedViewModel=ViewModelProviders.of(this).get(SharedViewModel.class);

        //Init UI==============================================================
    }
    public static void reduceMarginsInTabs(TabLayout tabLayout, int marginOffset) {

        View tabStrip = tabLayout.getChildAt(0);
        if (tabStrip instanceof ViewGroup) {
            ViewGroup tabStripGroup = (ViewGroup) tabStrip;
            for (int i = 0; i < ((ViewGroup) tabStrip).getChildCount(); i++) {
                View tabView = tabStripGroup.getChildAt(i);
                if (tabView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                    ((ViewGroup.MarginLayoutParams) tabView.getLayoutParams()).leftMargin = marginOffset;
                    ((ViewGroup.MarginLayoutParams) tabView.getLayoutParams()).rightMargin = marginOffset;
                }
            }

            tabLayout.requestLayout();
        }
    }

    public void wrapTabIndicatorToTitle(TabLayout tabLayout, int externalMargin, int internalMargin) {
        View tabStrip = tabLayout.getChildAt(0);
        if (tabStrip instanceof ViewGroup) {
            ViewGroup tabStripGroup = (ViewGroup) tabStrip;
            int childCount = ((ViewGroup) tabStrip).getChildCount();
            for (int i = 0; i < childCount; i++) {
                View tabView = tabStripGroup.getChildAt(i);
                //set minimum width to 0 for instead for small texts, indicator is not wrapped as expected
                tabView.setMinimumWidth(0);
                // set padding to 0 for wrapping indicator as title
                tabView.setPadding(0, tabView.getPaddingTop(), 0, tabView.getPaddingBottom());
                // setting custom margin between tabs
                if (tabView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) tabView.getLayoutParams();
                    if (i == 0) {
                        // left
                        settingMargin(layoutParams, externalMargin, internalMargin);
                    } else if (i == childCount - 1) {
                        // right
                        settingMargin(layoutParams, internalMargin, externalMargin);
                    } else {
                        // internal
                        settingMargin(layoutParams, internalMargin, internalMargin);
                    }
                }
            }


            tabLayout.requestLayout();
        }
    }

    private void settingMargin(ViewGroup.MarginLayoutParams layoutParams, int start, int end) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            layoutParams.setMarginStart(start);
            layoutParams.setMarginEnd(end);
        } else {
            layoutParams.leftMargin = start;
            layoutParams.rightMargin = end;
        }
    }

//    @Override
//    public void onAttachFragment(Fragment fragment) {
//        if(fragment instanceof FragmentSpeed2){
//            FragmentSpeed2 fragmentSpeed2=(FragmentSpeed2)fragment;
//            fragmentSpeed2.setSpeedTestCallBack(this);
//        }
//    }


    @Override
    public BackgroundTestService transportBackgroundTestService() {
        return mService;
    }
    @Override
    public BackgroundTestService2 transportBackgroundTestService2() {
        return mService2;
    }

    @Override
    public boolean getMBoundStatus() {
        return mBound;
    }

    @Override
    public void startBackgroundTestService2() {
        startService(intent);
    }


    @Override
    public void bindService() {
        bindService(intent,connection,Context.BIND_AUTO_CREATE);
    }

    @Override
    public void unbindService() {
        unbindService(connection);
    }

}
