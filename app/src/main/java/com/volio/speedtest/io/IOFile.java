package com.volio.speedtest.io;
import android.content.Context;
import android.util.Log;

import com.volio.speedtest.HistoryItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
public class IOFile {
    public static void readFile(List ls,String s,Context context){
        ls.clear();
        try{
            File directory=context.getCacheDir();
            File file=new File(directory,s);
            ObjectInputStream ois=new ObjectInputStream(new FileInputStream(file));
            Object ob=null;
            while((ob=ois.readObject())!=null){
                Object ob2=(Object)ob;
                ls.add(ob2);
            }
            ois.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("readFile","a");
    }
    public static void writeFile(List ls,String s,Context context){
        try
        {
            File directory=context.getCacheDir();
            File file=new File(directory,s);
            ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(file));
            for(Object ob:ls){
                oos.writeObject(ob);
            }
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("writeFile","a");
    }

}
